#!/bin/bash

gpu=$1
if [ -z "$gpu" ]; then gpu=0; fi

export train_cmd="utils/run.pl"
export decode_cmd="utils/run.pl"
export cuda_cmd="utils/run.pl --gpu $gpu"