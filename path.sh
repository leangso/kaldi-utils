#!/bin/bash

PROJ_DIR=${PWD}
LIB_DIR=../
KALDI_ROOT=${LIB_DIR}/kaldi

export PROJ_DIR=$PROJ_DIR
export LIB_DIR=$LIB_DIR
export KALDI_ROOT=$KALDI_ROOT

# init kaldi env
if [ -d $KALDI_ROOT ] && [ -f $KALDI_ROOT/.done ]; then
  . $KALDI_ROOT/tools/env.sh
  . $KALDI_ROOT/tools/config/common_path.sh

  export PATH=$PROJ_DIR/utils/:$KALDI_ROOT/tools/openfst/bin:$PROJ_DIR:$PATH
  export LC_ALL=C
fi
