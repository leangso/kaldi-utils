#!/usr/bin/env bash

. ./path.sh

# parameters
nproc=$1
if [ -z $nproc ]; then
  nproc=4
fi

if [ -f $KALDI_ROOT/.done ]; then
  echo "!!! Kaldi is installed..."
  exit 1
fi

echo "--> Install libraries..."
apt-get update && apt-get install -y \
    autoconf \
    automake \
    bzip2 \
    g++ \
    git \
    make \
    python2 \
    subversion \
    unzip \
    wget \
    sox \
    zlib1g-dev \
    gfortran \
    libtool || exit 1

echo "--> Clone Kaldi..."
if [ ! -d $KALDI_ROOT ]; then
  cd $LIB_DIR && git clone https://github.com/kaldi-asr/kaldi.git || exit 1
fi 

echo "--> Config Python..."
cd $KALDI_ROOT \
  && mkdir -p tools/python \
  && touch tools/python/.use_default_python || exit 1

echo "--> Install Intel MKL..."
# installer packages for Mac and Windows are available for download from Intel:
cd $KALDI_ROOT/tools && ./extras/install_mkl.sh || exit 1

echo "--> Check dependencies..."
output=`cd $KALDI_ROOT/tools && ./extras/check_dependencies.sh | grep 'not installed' | tr -s '\n' '#' `
if [ ! -z $output ]; then
  echo "!!! Please make sure the following dependencies are installed."
  echo $output | tr -s '#' '\n'
  exit 1
fi

echo "--> Build tools..."
cd $KALDI_ROOT/tools && make -j $nproc || exit 1

echo "--> Install Phonetisaurus..."
cd $KALDI_ROOT/tools && ./extras/install_phonetisaurus.sh || exit 1

echo "--> Install SRILM..."
cd $KALDI_ROOT/tools && ./extras/install_srilm.sh rnd cadt rnd@cadt.edu.kh || exit 1

echo "--> Build src..."
# config
if [ -z $CUDA_DIR ]; then
  cd $KALDI_ROOT/src && ./configure --shared
else
  cd $KALDI_ROOT/src && ./configure --shared --cudatk-dir=$CUDA_DIR
fi

# build src
cd $KALDI_ROOT/src &&
  make -j $nproc depend &&
  make -j $nproc || exit 1

touch $KALDI_ROOT/.done
