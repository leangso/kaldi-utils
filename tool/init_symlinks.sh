#!/usr/bin/env bash

. ./path.sh

[ ! -L $PROJ_DIR/steps ] && ln -s $KALDI_ROOT/egs/wsj/s5/steps $PROJ_DIR
[ ! -L $PROJ_DIR/utils ] && ln -s $KALDI_ROOT/egs/wsj/s5/utils $PROJ_DIR