#!/bin/bash

input=work/data       # data dir
output=work/result    # output dir
version=001
clean=false

stage=0

nj=10
nj_decode="4"

train_set=train
test_set="test"

cmvn_opts="--norm-means=true --norm-vars=false"
delta_opts="--delta-order=2"

# parse args
. ./utils/parse_options.sh

nj_decode_arr=($nj_decode)
test_set_arr=($test_set)

# load utils
. ./local/utils.sh

# load configuration
. ./conf/gmm.conf

# initialize PATH
. ./path.sh  || die "path.sh $expected";

# initialize commands
. ./cmd.sh

# init exp dir
exp=$output/exp_$version

# lang, lm
local=$exp/local
lang=$exp/lang
lm=$exp/lm

# model
model=$exp/model
mono=$model/mono
tri=$model/tri2a
lda=$model/tri2b
sat=$model/tri3b

[ "$clean" == true ] && rm -rf $exp

# make output directories
mkdir -p $exp $local $local/dict $lang $lm $model

# init logging
exec > >(tee -a $exp/log.txt)
exec 2>&1

# copy config files to exp dir
[ ! -d $exp/conf ] && cp -R conf $exp

#### Prepare Data ####
if [ $stage -le 1 ]; then
  printf ">>> 1. Preparing Data - $(date)"

  printf "\n--> Preparing Datasets\n"
  python3 local/prepare_data.py --data_dir $input/ds --output_dir $exp --dataset "$train_set $test_set" || exit 1

  printf "\n--> Preparing Lang\n"
  cp -R $input/dict $local || exit 1
  utils/prepare_lang.sh $local/dict "<unk>" $local/lang $lang || exit 1

  printf "\n--> Preparing LM\n"
  test_corpus=""
  for x in $train_set $test_set; do
    corpus_path=$exp/${x}/corpus.txt
    test_corpus+=" $corpus_path"
  done

  local/lm/prepare_lm.sh --train-set "$input/lm/corpus.txt" --test-set "$test_corpus" --output-dir $lm || exit 1
  arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $lm/model.lm.prune $lang/G.fst || exit 1
fi

#### Compute MFCC ####
if [ $stage -le 2 ]; then
  printf "\n>>> 2. Computing MFCC - $(date)\n"
  for x in $train_set $test_set; do
    utils/fix_data_dir.sh $exp/$x || exit 1

    steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $exp/$x || exit 1

    # no cmvn here, just generate cmvn.scp to work with kaldi
    # cmvn will be applied directly in scripts based on $cmvn_opts
    steps/compute_cmvn_stats.sh --fake $exp/$x || exit 1

    utils/validate_data_dir.sh $exp/$x || exit 1
  done
fi

#### Monophone ####
if [ $stage -le 3 ]; then
  printf "\n>>> 3. Starting Monophone - $(date)"

  printf "\n--> Monophone: Training\n"
  if [ -z $mono_num_examples ]; then
    steps/train_mono.sh --nj $nj --cmd "$train_cmd" --boost-silence 1.25 --cmvn_opts "$cmvn_opts" --delta-opts "$delta_opts" \
      $exp/$train_set $lang $mono || exit 1
  else
    utils/subset_data_dir.sh $exp/$train_set $mono_num_examples $exp/$train_set.$mono_num_examples || exit 1
    utils/fix_data_dir.sh $exp/$train_set.$mono_num_examples || exit 1
    
    steps/train_mono.sh --nj $nj --cmd "$train_cmd" --boost-silence 1.25 --cmvn_opts "$cmvn_opts" --delta-opts "$delta_opts" \
      $exp/$train_set.$mono_num_examples $lang $mono || exit 1
  fi

  printf "\n--> Monophone: Make Graph\n"
  utils/mkgraph.sh $lang $mono $mono/graph || exit 1

  if [ "$mono_decode" == true ]; then
    printf "\n--> Monophone: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode.sh --config conf/decode.conf --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
        $mono/graph $exp/${test_set_arr[$i]} $mono/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

#### Triphone ####
if [ $stage -le 4 ]; then
  printf "\n>>> 4. Starting Triphone - $(date)"

  printf "\n--> Monophone: Alignment\n"
  steps/align_si.sh --nj $nj --cmd "$train_cmd" --boost-silence 1.25 $exp/$train_set $lang $mono ${mono}_ali || exit 1

  printf "\n--> Triphone: Training\n"
  steps/train_deltas.sh --cmd "$train_cmd"--cmvn_opts "$cmvn_opts" --delta-opts "$delta_opts" \
    $tri_num_leaves $tri_num_gauss $exp/$train_set $lang ${mono}_ali $tri || exit 1

  printf "\n--> Triphone: Make Graph\n"
  utils/mkgraph.sh $lang $tri $tri/graph || exit 1

  if [ "$tri_decode" == true ]; then
    printf "\n--> Triphone: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode.sh --config conf/decode.conf --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
        $tri/graph $exp/${test_set_arr[$i]} $tri/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

#### LDA-MLLT ####
if [ $stage -le 5 ]; then
  printf "\n>>> 5. Starting LDA-MLLT - $(date)"

  printf "\n--> Triphone: Alignment\n"
  steps/align_si.sh --nj $nj --cmd "$train_cmd" $exp/$train_set $lang $tri ${tri}_ali || exit 1

  printf "\n--> LDA-MLLT: Training\n"
  steps/train_lda_mllt.sh --cmd "$train_cmd" --splice-opts "--left-context=3 --right-context=3" \
    $lda_num_leaves $lda_num_gauss $exp/$train_set $lang ${tri}_ali $lda || exit 1

  printf "\n--> LDA-MLLT: Make Graph\n"
  utils/mkgraph.sh $lang $lda $lda/graph || exit 1

  if [ "$lda_decode" == true ]; then
    printf "\n--> LDA-MLLT: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode.sh --config conf/decode.conf --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
        $lda/graph $exp/${test_set_arr[$i]} $lda/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

#### SAT ####
if [ $stage -le 6 ]; then
  printf "\n>>> 6. Starting SAT - $(date)"

  printf "\n--> LDA-MLLT: Alignment\n"
  steps/align_si.sh --nj $nj --cmd "$train_cmd" $exp/$train_set $lang $lda ${lda}_ali || exit 1

  printf "\n--> SAT: Training\n"
  steps/train_sat.sh --cmd "$train_cmd" $sat_num_leaves $sat_num_gauss $exp/$train_set $lang ${lda}_ali $sat || exit 1

  printf "\n--> SAT: Make Graph\n"
  utils/mkgraph.sh $lang $sat $sat/graph || exit 1
  
  if [ "$sat_decode" == true ]; then
    printf "\n--> SAT: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode_fmllr.sh --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
        $sat/graph $exp/${test_set_arr[$i]} $sat/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

#### SGMM ####
if [ $stage -le 7 ]; then
  printf "\n>>> 7. Starting SGMM - $(date)"

  printf "\n--> SAT: Alignment\n"
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" $train $lang $sat ${sat}_ali || exit 1

  printf "\n--> SGMM: Training\n"
  steps/train_ubm.sh --cmd "$train_cmd" $sgmm_ubm_num_gauss $train $lang ${sat}_ali $ubm || exit 1
  steps/train_sgmm2.sh --cmd "$train_cmd" $sgmm_num_leaves $sgmm_num_gauss \
    $train $lang ${sat}_ali $ubm/final.ubm $sgmm || exit 1

  printf "\n--> SGMM: Make Graph\n"
  utils/mkgraph.sh $lang $sgmm $sgmm/graph || exit 1
  
  if [ "$sgmm_decode" == true ]; then
    printf "\n--> SGMM: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode_sgmm2.sh --nj ${nj_decode[$i]} --cmd "$decode_cmd" --transform-dir $sat/decode_${test_set_arr[$i]} \
        $sgmm/graph $exp/${test_set_arr[$i]} $sgmm/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

# wait background process
wait

printf "\n>>> WER - $(date)\n"
compute_wer $model/*/decode*