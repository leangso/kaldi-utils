#!/bin/bash

input=~/work/exp/data/french       # data dir
output=~/work/exp/result/french    # output dir

version=001
clean=false

stage=0

nj=15
nj_decode="10"

train_set=train
test_set="test"

cmvn_opts="--norm-means=false --norm-vars=false"
delta_opts="--delta-order=2"

# parse args
. ./utils/parse_options.sh

nj_decode_arr=($nj_decode)
test_set_arr=($test_set)

# load utils
. ./local/utils.sh

# load configuration
. ./conf/gmm.conf

# initialize PATH
. ./path.sh

# initialize commands
. ./cmd.sh

# init exp dir
exp=$output/exp_$version

# lang, lm
local=$exp/local
lang=$exp/lang
lm=$exp/lm

# model
model=$exp/model
mono=$model/mono
tri=$model/tri2a

[ "$clean" == true ] && rm -rf $exp

# make output directories
mkdir -p $exp $local $local/dict $lang $lm $model

# init logging
exec > >(tee -a $exp/log.txt)
exec 2>&1

start_time=$(date +%s)
printf "\n>>> Start time: $(date)\n"

# copy config files to exp dir
[ ! -d $exp/conf ] && cp -R conf $exp

#### Prepare Data ####
if [ $stage -le 1 ]; then
  printf ">>> 1. Preparing Data - $(date)"

  printf "\n--> Preparing Datasets\n"
  python3 local/prepare_data_v2.py --data_dir $input/ds --output_dir $exp --dataset "$train_set $test_set" || exit 1

  printf "\n--> Preparing Lang\n"
  cp -R $input/dict $local || exit 1
  utils/prepare_lang.sh $local/dict "<unk>" $local/lang $lang || exit 1

  printf "\n--> Preparing LM\n"
  test_corpus=""
  for x in $train_set $test_set; do
    corpus_path=$exp/${x}/corpus.txt
    test_corpus+=" $corpus_path"
  done
  
  local/lm/prepare_lm.sh --train-set "$input/lm/corpus.txt" --test-set "$test_corpus" --order 3 --discount "" --output-dir $lm || exit 1
  arpa2fst --disambig-symbol=#0 --read-symbol-table=$lang/words.txt $lm/model.lm $lang/G.fst || exit 1
fi

#### Compute MFCC ####
if [ $stage -le 2 ]; then
  printf "\n>>> 2. Computing MFCC - $(date)\n"
  for x in $train_set $test_set; do
    utils/fix_data_dir.sh $exp/$x || exit 1

    steps/make_mfcc.sh --nj $nj --cmd "$train_cmd" $exp/$x || exit 1

    # no cmvn here, just generate cmvn.scp to work with kaldi
    # cmvn will be applied directly in scripts based on $cmvn_opts
    steps/compute_cmvn_stats.sh --fake $exp/$x || exit 1

    utils/validate_data_dir.sh $exp/$x || exit 1
  done
fi

#### Monophone ####
if [ $stage -le 3 ]; then
  printf "\n>>> 3. Starting Monophone - $(date)"

  printf "\n--> Monophone: Training\n"
  steps/train_mono.sh --nj $nj --cmd "$train_cmd" --cmvn_opts "$cmvn_opts" --delta-opts "$delta_opts" \
    $exp/$train_set $lang $mono || exit 1
  
  printf "\n--> Monophone: Make Graph\n"
  utils/mkgraph.sh $lang $mono $mono/graph || exit 1

  if [ "$mono_decode" == true ]; then
    printf "\n--> Monophone: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode.sh --config conf/decode.conf --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
        $mono/graph $exp/${test_set_arr[$i]} $mono/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

#### Triphone ####
if [ $stage -le 4 ]; then
  printf "\n>>> 4. Starting Triphone - $(date)"

  printf "\n--> Monophone: Alignment\n"
  steps/align_si.sh --nj $nj --cmd "$train_cmd" $exp/$train_set $lang $mono ${mono}_ali || exit 1

  printf "\n--> Triphone: Training\n"
  steps/train_deltas.sh --cmd "$train_cmd" --cmvn_opts "$cmvn_opts" --delta-opts "$delta_opts" \
    $tri_num_leaves $tri_num_gauss $exp/$train_set $lang ${mono}_ali $tri || exit 1

  printf "\n--> Triphone: Make Graph\n"
  utils/mkgraph.sh $lang $tri $tri/graph || exit 1

  if [ "$tri_decode" == true ]; then
    printf "\n--> Triphone: Decoding\n"
    for i in "${!test_set_arr[@]}"; do
      steps/decode.sh --config conf/decode.conf --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
        $tri/graph $exp/${test_set_arr[$i]} $tri/decode_${test_set_arr[$i]} || exit 1
    done
  fi
fi

#### NNET2 ####
if [ $stage -le 5 ]; then
  printf "\n>>> 5. Starting DNN - $(date)"
  
  printf "\n--> Triphone: Alignment\n"
  steps/align_si.sh --nj $nj --cmd "$train_cmd" $exp/$train_set $lang $tri ${tri}_ali || exit 1

  printf "\n--> DNN: Training\n"
  local/nnet2/run_5c.sh --nj $nj --nj_decode "$nj_decode" \
    --train-set $train_set --test-set "$test_set" \
    --exp $exp --gmm ${tri} --output $model || exit 1
fi

# wait background process
wait

printf "\n>>> WER - $(date)\n"
compute_wer $model/*/decode* $model/nnet2/*/decode*

printf "\n<<< End time: $(date)"
print_exp_time $(date +%s) $start_time