#!/usr/bin/env bash

# the script is from iban/local/nnet3/tuning/run_tdnn_1a.sh
# - network config follows wsj/local/nnet3/tuning/run_tdnn_1a.sh

# this is the standard "tdnn" system, built in nnet3; it's what we use to
# call multi-splice.

# At this script level we don't support not running on GPU, as it would be painfully slow.
# If you want to run without GPU you'd have to call train_tdnn.sh with --gpu false,
# --num-threads 16 and --minibatch-size 128.

exp=
gmm=$exp/model/tri3b  # sat
output=

stage=0
ivector_stage=0

nj=10
nj_decode="4"

train_set=train
test_set="test"

ivector_dim=100
train_stage=-10
affix=
common_egs_dir=
remove_egs=true

. ./utils/parse_options.sh
. ./cmd.sh
. ./path.sh

nj_decode_arr=($nj_decode)
test_set_arr=($test_set)

gmm_model=`basename $gmm`
output=${output}/tdnn${affix}

# init logging
exec > >(tee -i ${output}/tdnn.log)
exec 2>&1

if ! cuda-compiled; then
  cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
fi

ali_dir=$output/${gmm_model}_ali_${train_set}_sp
dir=$output
train_data_dir=$exp/${train_set}_sp_hires
train_ivector_dir=$output/ivectors_${train_set}_sp_hires

if [ $stage -le 0 ]; then
  local/nnet3/run_ivector_common_b.sh --stage $ivector_stage=0 \
                                  --train-set $train_set \
                                  --test-set $test_set \
                                  --nj $nj --nj-decode $nj_decode \
                                  --ivector-dim $ivector_dim \
                                  --exp $exp --gmm $gmm --output $dir || exit 1;  
fi

if [ $stage -le 1 ]; then
  echo "$0: creating neural net configs";

  num_targets=$(tree-info $ali_dir/tree | grep num-pdfs | awk '{print $2}')

  mkdir -p $dir/configs
  cat <<EOF > $dir/configs/network.xconfig
  input dim=100 name=ivector
  input dim=40 name=input

  # please note that it is important to have input layer with the name=input
  # as the layer immediately preceding the fixed-affine-layer to enable
  # the use of short notation for the descriptor
  fixed-affine-layer name=lda input=Append(-2,-1,0,1,2,ReplaceIndex(ivector, t, 0)) affine-transform-file=$dir/configs/lda.mat

  # the first splicing is moved before the lda layer, so no splicing here
  relu-renorm-layer name=tdnn1 dim=512
  relu-renorm-layer name=tdnn2 dim=512 input=Append(-1,0,1)
  relu-renorm-layer name=tdnn3 dim=512 input=Append(-1,0,1)
  relu-renorm-layer name=tdnn4 dim=512 input=Append(-3,0,3)
  relu-renorm-layer name=tdnn5 dim=512 input=Append(-6,-3,0)
  output-layer name=output dim=$num_targets max-change=1.5
EOF

  steps/nnet3/xconfig_to_configs.py --xconfig-file $dir/configs/network.xconfig --config-dir $dir/configs/
fi

if [ $stage -le 2 ]; then
  steps/nnet3/train_dnn.py --stage $train_stage \
    --cmd="$decode_cmd" \
    --trainer.optimization.num-jobs-initial 2 \
    --trainer.optimization.num-jobs-final $nj \
    --trainer.num-epochs 3 \
    --feat.online-ivector-dir=$train_ivector_dir \
    --feat.cmvn-opts "--norm-means=false --norm-vars=false" \
    --trainer.optimization.initial-effective-lrate 0.005 \
    --trainer.optimization.final-effective-lrate 0.0005 \
    --trainer.samples-per-iter 120000 \
    --egs.dir "$common_egs_dir" \
    --cleanup.preserve-model-interval 10 \
    --cleanup.remove-egs=$remove_egs \
    --feat-dir=$train_data_dir \
    --ali-dir=$ali_dir \
    --lang $exp/lang \
    --use-gpu=wait \
    --dir=$dir  || exit 1;
fi

if [ $stage -le 3 ]; then
  # this does offline decoding that should give the same results as the real
  # online decoding.
  # use already-built graphs.
  for i in "${!test_set_arr[@]}"; do
    data=${test_set_arr[$i]}
    
    steps/nnet3/decode.sh --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
    --online-ivector-dir $tdnn/ivectors_test_hires --iter final \
    $gmm/graph $exp/${data}_hires $dir/decode_${data} || exit 1;
  done
fi

exit 0;

