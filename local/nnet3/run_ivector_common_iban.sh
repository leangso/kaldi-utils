#!/usr/bin/env bash

# the script is from iban/local/nnet3/run_ivector_common.sh

set -euo pipefail

# This script is called from local/nnet3/run_tdnn.sh and
# local/chain/run_tdnn.sh (and may eventually be called by more
# scripts).  It contains the common feature preparation and
# iVector-related parts of the script.  See those scripts for examples
# of usage.

exp=
gmm=$exp/model/tri3b  # sat
output=

stage=0

nj=10
nj_decode="4"

train_set=train
test_set="test"

ivector_dim=100
online_cmvn_iextractor=false

. ./utils/parse_options.sh
. ./cmd.sh
. ./path.sh

nj_decode_arr=($nj_decode)
test_set_arr=($test_set)

gmm_model=`basename $gmm`
ali_dir=$output/${gmm_model}_ali_${train_set}_sp

for f in $exp/${train_set}/feats.scp ${gmm}/final.mdl; do
  if [ ! -f $f ]; then
    echo "$0: expected file $f to exist"
    exit 1
  fi
done

if [ $stage -le 1 ]; then
  # Although the nnet will be trained by high resolution data, we still have to
  # perturb the normal data to get the alignment _sp stands for speed-perturbed
  echo "$0: preparing directory for low-resolution speed-perturbed data (for alignment)"
  utils/data/perturb_data_dir_speed_3way.sh $exp/${train_set} $exp/${train_set}_sp
  
  echo "$0: making MFCC features for low-resolution speed-perturbed data"
  steps/make_mfcc.sh --cmd "$train_cmd" --nj $nj $exp/${train_set}_sp || exit 1;
  steps/compute_cmvn_stats.sh $exp/${train_set}_sp || exit 1;
  
  utils/fix_data_dir.sh $exp/${train_set}_sp
fi

if [ $stage -le 2 ]; then
  echo "$0: aligning with the perturbed low-resolution data"
  steps/align_fmllr.sh --nj $nj --cmd "$train_cmd" \
    $exp/${train_set}_sp $exp/lang $gmm $ali_dir || exit 1
fi

if [ $stage -le 3 ]; then
  # Create high-resolution MFCC features (with 40 cepstra instead of 13).
  # this shows how you can split across multiple file-systems.
  echo "$0: creating high-resolution MFCC features"
  
  for datadir in ${train_set}_sp $test_set; do
    utils/copy_data_dir.sh $exp/$datadir $exp/${datadir}_hires
  done

  # do volume-perturbation on the training data prior to extracting hires
  # features; this helps make trained nnets more invariant to test data volume.
  utils/data/perturb_data_dir_volume.sh $exp/${train_set}_sp_hires || exit 1;

  steps/make_mfcc.sh --nj $nj --mfcc-config conf/mfcc_hires.conf --cmd "$train_cmd" $exp/${train_set}_sp_hires || exit 1;
  steps/compute_cmvn_stats.sh $exp/${train_set}_sp_hires || exit 1;
  utils/fix_data_dir.sh $exp/${train_set}_sp_hires || exit 1;
  
  # test
  for i in "${!test_set_arr[@]}"; do
    datadir=${test_set_arr[$i]}
    steps/make_mfcc.sh --nj ${nj_decode_arr[$i]} --mfcc-config conf/mfcc_hires.conf --cmd "$train_cmd" $exp/${datadir}_hires || exit 1;
    steps/compute_cmvn_stats.sh $exp/${datadir}_hires || exit 1;
    utils/fix_data_dir.sh $exp/${datadir}_hires || exit 1;
  done
fi

if [ $stage -le 4 ]; then
  # Train a small system just for its LDA+MLLT transform.  We use --num-iters 13
  # because after we get the transform (12th iter is the last), any further
  # training is pointless.
  steps/train_lda_mllt.sh --cmd "$train_cmd" --num-iters 13 \
    --realign-iters "" --splice-opts "--left-context=3 --right-context=3" \
    5000 10000 $exp/${train_set}_sp_hires $exp/lang \
    $ali_dir $output/${gmm_model} || exit 1
fi

if [ $stage -le 5 ]; then
  echo "$0: training the diagonal UBM."
  # Use 512 Gaussians in the UBM.
  steps/online/nnet2/train_diag_ubm.sh --cmd "$train_cmd" --nj $nj --num-frames 200000 \
    $exp/${train_set}_sp_hires 512 $output/${gmm_model} $output/diag_ubm || exit 1
fi

if [ $stage -le 6 ]; then
  # Train the iVector extractor.  Use all of the speed-perturbed data since iVector extractors
  # can be sensitive to the amount of data.  The script defaults to an iVector dimension of
  # 100.
  echo "$0: training the iVector extractor"
  steps/online/nnet2/train_ivector_extractor.sh --cmd "$train_cmd" --nj $nj \
     --num-threads 2 --num-processes 2 \
     --ivector-dim $ivector_dim \
     --online-cmvn-iextractor $online_cmvn_iextractor \
     $exp/${train_set}_sp_hires $output/diag_ubm \
     $output/extractor || exit 1;
fi

if [ $stage -le 7 ]; then
  # We extract iVectors on the speed-perturbed training data after combining
  # short segments, which will be what we train the system on.  With
  # --utts-per-spk-max 2, the script pairs the utterances into twos, and treats
  # each of these pairs as one speaker; this gives more diversity in iVectors..
  # Note that these are extracted 'online'.

  # note, we don't encode the 'max2' in the name of the ivectordir even though
  # that's the data we extract the ivectors from, as it's still going to be
  # valid for the non-'max2' data, the utterance list is the same.

  ivectordir=$output/ivectors_${train_set}_sp_hires

  # having a larger number of speakers is helpful for generalization, and to
  # handle per-utterance decoding well (iVector starts at zero).
  temp_data_root=${ivectordir}
  utils/data/modify_speaker_info.sh --utts-per-spk-max 2 \
    $exp/${train_set}_sp_hires ${temp_data_root}/${train_set}_sp_hires_max2

  steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj $nj \
    ${temp_data_root}/${train_set}_sp_hires_max2 \
    $output/extractor $ivectordir

  # Also extract iVectors for the test data, but in this case we don't need the speed
  # perturbation (sp).
  for i in "${!test_set_arr[@]}"; do
    data=${test_set_arr[$i]}
    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj ${nj_decode_arr[$i]} \
      $exp/${data}_hires $output/extractor \
      $output/ivectors_${data}_hires
  done
fi

exit 0
