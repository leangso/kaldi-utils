#!/bin/bash

second2time() {
    seconds=$1
    echo $((seconds/86400))" days "$(date -d "1970-01-01 + $seconds seconds" "+%H hours %M minutes %S seconds")
}

print_exp_time() {
    start_time=$1
    end_time=$2
    printf '\nTme spent: %s\n' "$(second2time $(($end_time - $start_time)))"
}

compute_wer() {
    for x in "$@"; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
}