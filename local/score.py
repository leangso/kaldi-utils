import click
import os

from pathlib import Path
from glob import glob
from os import path
from jiwer import wer as WER


def compute_wer(exp_dir, combine_wers=False):
    wer_dir = path.join(exp_dir, 'wer')
    os.makedirs(wer_dir, exist_ok=True)

    ## ref
    refs_map = {}
    for test_dir in glob(exp_dir + '/test*', recursive=True):
        dataset = os.path.basename(test_dir)

        refs = {}
        with open(path.join(test_dir, 'text'), 'r', encoding='utf-8') as reader:
            for line in reader:
                parts = line.strip().split(' ', 1)
                utt_id, trans = parts[0], parts[1]
                refs[utt_id] = trans

        refs_map[dataset] =refs

    ## hyp
    hyps_map = {}
    for decode_dir in sorted(glob(path.join(exp_dir, 'model') + '/**/decode_*', recursive=True)):
        model = path.basename(str(Path(decode_dir).parent))
        dataset = path.basename(decode_dir).replace('decode_', '')

        refs = refs_map[dataset]
        if refs is None:
            raise Exception('Testing dataset not found: ', dataset)

        model_hyps_map = hyps_map.get(model)
        if model_hyps_map is None:
            model_hyps_map = {}

        scoring_dir = path.join(decode_dir, 'scoring_kaldi')

        with open('%s/wer_details/lmwt' % scoring_dir) as reader:
            best_lmwt = reader.read().strip()

        with open('%s/wer_details/wip' % scoring_dir) as reader:
            best_wip = reader.read().strip()

        hyps = {}
        decode_file = '%s/penalty_%s/%s.txt' % (scoring_dir, best_wip, best_lmwt)
        with open(decode_file, 'r', encoding='utf-8') as reader:
            for line in reader:
                parts = line.strip().split(' ', 1)
                utt_id = parts[0]
                trans = '' if len(parts) == 1 else parts[1]
                
                hyps[utt_id] = trans

        model_hyps_map[dataset] = {'hyps':  hyps, 'decode_file': decode_file}
        hyps_map[model] = model_hyps_map

    ## compute wer
    log_file = open(path.join(wer_dir, 'log.txt'), 'w', encoding='utf-8')

    for model, model_hyps_map in sorted(hyps_map.items()):
        print('model: ', model)
        
        log_file.write('model: %s\n' % model)

        out_model_dir = path.join(wer_dir, model)
        os.makedirs(out_model_dir, exist_ok=True)

        if combine_wers:
            all_order_refs_ = []
            all_order_hyps_ = []
            
        for dataset, eles in model_hyps_map.items():
            hyps_ = eles['hyps']
            decode_file = eles['decode_file']

            refs_ = refs_map[dataset]

            # write decode file
            decode_file_writer = open(path.join(out_model_dir, dataset + '.csv'), 'w', encoding='utf-8')
            for utt_id, hyp in hyps_.items():
                ref = refs_[utt_id]
                decode_file_writer.write('%s , %s , %s\n' % (utt_id, ref, hyp))
            decode_file_writer.close()

            # write wer
            order_refs_ = []
            order_hyps_ = []
            for utt_id, hyp_ in hyps_.items():
                ref_ = refs_[utt_id]
                order_refs_.append(ref_)
                order_hyps_.append(hyp_)

            if combine_wers:
                all_order_refs_.extend(order_refs_)
                all_order_hyps_.extend(order_hyps_)

            wer = 100 * WER(order_refs_, order_hyps_)
                
            summary = "{:<15}: wer={:05.2f}, decode file={:}\n".format(dataset, wer, decode_file)
            log_file.write(summary)
            print(summary)

        if combine_wers:
            wer = 100 * WER(all_order_refs_, all_order_hyps_)
            summary = "{:<15}: wer={:05.2f}".format('total', wer)
            log_file.write(summary)
            print(summary)

        log_file.write('\n')
        print()

    log_file.close()

        
@click.command()
@click.option('--exp_dir', type=str)
@click.option('--combine_wers', type=bool, default=False)
def main(exp_dir, combine_wers):
    compute_wer(exp_dir, combine_wers)


if __name__ == '__main__':
    main()
