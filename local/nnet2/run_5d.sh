#!/usr/bin/env bash

# This is pnorm neural net training on top of adapted 40-dimensional features.


train_stage=-10
use_gpu=true

exp=
gmm=
output=


nj=10
nj_decode="4"

train_set=train
test_set="test"


cmvn_opts=
delta_opts=


num_hidden_layers=3     
pnorm_input_dim=2000     # 2000
pnorm_output_dim=400     # 400

minibatch_size=128
initial_learning_rate=0.01  # 0.02
final_learning_rate=0.001   # 0.004
num_epochs=15


. ./utils/parse_options.sh
. ./cmd.sh
. ./path.sh

nj_decode_arr=($nj_decode)
test_set_arr=($test_set)

output=$output/nnet2
mkdir -p $output

if $use_gpu; then
  if ! cuda-compiled; then
    cat <<EOF && exit 1
This script is intended to be used with GPUs but you have not compiled Kaldi with CUDA
If you want to use GPUs (and have them), go to src/, and configure and make on a machine
where "nvcc" is installed.
EOF
  fi
  parallel_opts="--gpu $gpu"
  num_threads=1
  # minibatch_size=512
  dir=$output/nnet5d
else
  # Use 4 nnet jobs just like run_4d_gpu.sh so the results should be
  # almost the same, but this may be a little bit slow.
  num_threads=15
  parallel_opts="--num-threads $num_threads"
  # minibatch_size=128
  dir=$output/nnet5d
fi


if [ ! -f $dir/final.mdl ]; then
  steps/nnet2/train_pnorm_fast.sh --stage $train_stage \
   --samples-per-iter 400000 \
   --parallel-opts "$parallel_opts" \
   --num-threads $num_threads \
   --minibatch-size "$minibatch_size" \
   --num-jobs-nnet 1  --mix-up 8000 \
   --initial-learning-rate $initial_learning_rate --final-learning-rate $final_learning_rate \
   --num-hidden-layers $num_hidden_layers --pnorm_input_dim $pnorm_input_dim --pnorm_output_dim $pnorm_output_dim \
   --num_epochs $num_epochs \
   --cmd "$decode_cmd" \
   --cmvn_opts "$cmvn_opts" --delta-opts "$delta_opts" \
     $exp/${train_set} $exp/lang ${gmm}_ali $dir || exit 1
fi

for i in "${!test_set_arr[@]}"; do
  steps/nnet2/decode.sh --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" \
    --transform-dir "$transform_dir" \
    $gmm/graph $exp/${test_set_arr[$i]} $dir/decode_${test_set_arr[$i]} || exit 1
done


wait;

