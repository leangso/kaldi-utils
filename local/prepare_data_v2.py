import os
import subprocess

import click


PROJ_HOME = os.environ['PROJ_DIR']


def read_dataset(data_dir, dataset_name, skip_utt=[]):
    if not os.path.exists(data_dir):
        raise Exception('Data directory not found: ', data_dir)

    trans_file = os.path.join(data_dir, dataset_name + '.csv')
    if not os.path.exists(trans_file):
        raise Exception('Transcription file not found: ', trans_file)

    dataset = []
    with open(trans_file, mode='r', encoding='utf-8') as reader:
        for line in reader:
            ele = line.split('\t', 1)

            utt_id = ele[0]
            if utt_id.startswith('#') or utt_id in skip_utt:
                continue
            
            parts = utt_id.split('-')    # spk_id-session_id-trans_id
            if len(parts) == 3:
                spk_id, session_id, trans_id = parts[0], parts[1], parts[2]
            elif len(parts) == 2:
                session_id = None
                spk_id, trans_id = parts[0], parts[1]

            trans = ele[1].strip()

            dataset.append((utt_id, spk_id, session_id, trans_id, trans))
    return dataset


def generate_dataset(data, data_dir, output_dir):
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    # write data
    with open(output_dir + '/text', mode='w', encoding='utf-8') as text_writer, \
            open(output_dir + '/wav.scp', mode='w', encoding='utf-8') as wav_writer, \
            open(output_dir + '/utt2spk', mode='w', encoding='utf-8') as utt2spk_writer:

        invalid_examples = []
        for utt_id, spk_id, session_id, trans_id, trans in data:
            if session_id is None:
                wav_path = os.path.abspath(os.path.join(data_dir, 'wav', spk_id, trans_id + '.wav'))
            else:
                wav_path = os.path.abspath(os.path.join(data_dir, 'wav', spk_id, session_id + '-' + trans_id + '.wav'))
            if not os.path.exists(wav_path):
                print('!!> Wave file not found: %s' % wav_path)
                invalid_examples.append(utt_id)
                continue

            text_writer.write('%s %s\n' % (utt_id, trans))
            wav_writer.write('%s %s\n' % (utt_id, wav_path))

        for utt_id, spk_id, session_id, trans_id, trans in data:
            if utt_id in invalid_examples:
                continue

            utt2spk_writer.write('%s %s\n' % (utt_id, spk_id))

    with open(output_dir + '/spk2utt', mode='w', encoding='utf-8') as spk2utt_writer:
        subprocess.call(
            ['%s/utils/utt2spk_to_spk2utt.pl' % PROJ_HOME, '%s/utt2spk' % output_dir], stdout=spk2utt_writer)


def generate_text_corpus(dataset, output_dir):
    corpus = []
    for utt_id, spk_id, session_id, trans_id, trans in dataset:
        corpus.append(trans)

    with open('%s/corpus.txt' % output_dir, 'w', encoding='utf-8') as writer:
        corpus = set(corpus)
        corpus = list(corpus)
        corpus.sort()

        for line in corpus:
            writer.write('<s> %s </s>\n' % line)


def read_skip_utt(data_dir):
    skip_list = []
    skip_file = '%s/skip.txt' % data_dir
    if os.path.exists(skip_file):
        with open(skip_file, 'r', encoding='utf-8') as reader:
            for line in reader:
                line = line.strip()
                skip_list.append(line)
    return skip_list


@click.command()
@click.option('--data_dir', type=str)
@click.option('--output_dir', type=str)
@click.option('--dataset', type=str)
def main(data_dir, output_dir, dataset):
    skip_utt = read_skip_utt(data_dir)

    for name in dataset.split():
        print("Generate dataset: %s" % (os.path.join(data_dir, name)))
        data = read_dataset(data_dir, name, skip_utt)

        generate_dataset(data, data_dir, os.path.join(output_dir, name))
        generate_text_corpus(data, os.path.join(output_dir, name))


if __name__ == '__main__':
    main()
