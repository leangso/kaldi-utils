#!/usr/bin/env bash

# parameters
lexicon_file=$1
model_dir=$2

dir=`dirname "$0"`

# check parameters
[ -z $lexicon_file ] && echo >&2 "lexicon_file is undefined" && exit 1

mkdir -p $model_dir

if type -t phonetisaurus > /dev/null; then
    phonetisaurus train --model $model_dir/g2p.fst $lexicon_file
else
    phonetisaurus-train --lexicon $lexicon_file --dir_prefix $model_dir --verbose
fi

python3 $dir/generate_phones.py --lexicon_file $lexicon_file --output_file $model_dir/phones.txt