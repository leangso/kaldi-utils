import re
from collections import Counter
from operator import itemgetter
import argparse


def generate_phones(lexicon_file, vocab_file):
    phone_counter = Counter()
    print('Read lexicon file: %s' % lexicon_file)
    with open(lexicon_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            phones = line.strip().split()[1:]
            phone_counter.update(phones)

    # write phone
    print('Write phone file: %s ' % vocab_file)
    with open(vocab_file, 'w', encoding='utf-8') as writer:
        lexicon = sorted(phone_counter.items(), key=itemgetter(0))
        for word, n in lexicon:
            if word in ['<s>', '</s>']:
                continue
            writer.write('%s\n' % word)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--lexicon_file', type=str)
    parser.add_argument('--output_file', type=str)
    
    args = parser.parse_args()
    
    generate_phones(args.lexicon_file, args.output_file)