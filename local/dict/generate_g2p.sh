#!/usr/bin/env bash

# parameters
model_file=$1
file_in=$2
file_out=$3

# check parameters
[ -z $model_file ] && echo >&2 "model file is undefined" && exit 1
[ -z $file_in ] && echo >&2 "file_in is undefined" && exit 1
[ -z $file_out ] && echo >&2 "file_out is undefined" && exit 1

if type -t phonetisaurus > /dev/null; then
    cat $file_in | phonetisaurus predict --model $model_file > $file_out
else
    phonetisaurus-apply --model $model_file --word_list $file_tmp > $file_out
fi