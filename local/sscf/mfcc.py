import numpy as np
import python_speech_features as psf

from python_speech_features import sigproc
from scipy.fftpack import dct

import util as util


def compute_mfccs(signal, sample_rate=16000, win_len=0.025, win_step=0.01, num_cep=13, num_filt=26, num_fft=512,
                  low_freq=0, high_freq=None, preemph=0.97, lifter=22, append_energy=True, win_func=np.hamming):
    signal = sigproc.preemphasis(signal, preemph)
    frames = sigproc.framesig(signal, win_len * sample_rate, win_step * sample_rate, win_func)

    pspec = sigproc.powspec(frames, num_fft)
    energy = np.sum(pspec, 1)  # this stores the total energy in each frame
    energy = np.where(energy == 0, np.finfo(float).eps, energy)  # if energy is zero, we get problems with log

    fb = psf.get_filterbanks(num_filt, num_fft, sample_rate, low_freq, high_freq)
    feat = np.dot(pspec, fb.T)  # compute the filterbank energies
    feat = np.where(feat == 0, np.finfo(float).eps, feat)  # if feat is zero, we get problems with log

    feat = np.log(feat)
    feat = dct(feat, type=2, axis=1, norm='ortho')[:, :num_cep]
    feat = psf.lifter(feat, lifter)
    if append_energy: feat[:, 0] = np.log(energy)  # replace first cepstral coefficient with log of frame energy

    return feat


def delta(feat, N):
    if N < 1:
        raise ValueError('N must be an integer >= 1')
    NUMFRAMES = len(feat)
    denominator = 2 * sum([i ** 2 for i in range(1, N + 1)])
    delta_feat = np.empty_like(feat)
    padded = np.pad(feat, ((N, N), (0, 0)), mode='edge')   # padded version of feat
    for t in range(NUMFRAMES):
        delta_feat[t] = np.dot(np.arange(-N, N + 1), padded[t : t + 2 * N + 1]) / denominator   # [t : t+2*N+1] == [(N+t)-N : (N+t)+N+1]
    return delta_feat