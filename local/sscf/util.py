import librosa
import librosa.display
import matplotlib.pyplot as plt
import numpy as np
import parselmouth
import python_speech_features as psf

from python_speech_features import sigproc
from scipy.io import wavfile as wav


def read_wav(file):
    rate, signal = wav.read(file)
    return rate, signal


def get_frames(signal,
               sample_rate=16000,
               win_len=0.025,
               win_step=0.01,
               preemph=0.97,
               win_func=np.hamming):

    signal = sigproc.preemphasis(signal, preemph)
    frames = sigproc.framesig(sig=signal, frame_len=win_len * sample_rate, frame_step=win_step * sample_rate, winfunc=win_func)
    return frames


# input: NxD, output: Nx(NFFT/2+1)
def get_power_spectrum(frames, nfft=512):
    pspec = sigproc.powspec(frames, nfft)
    pspec = np.where(pspec == 0, np.finfo(float).eps, pspec)
    return pspec


def power2decibel(power_spectrum):
    return 10 * np.log10(power_spectrum)


def delta(frames, delta_step=1):
    padded = np.pad(frames, ((delta_step, 0), (0, 0)), mode='edge')
    delta = padded[delta_step:] - padded[:-delta_step]
    return delta


# mfcc delta
def delta_mfcc(frames, delta_step=1):
    deltas = psf.delta(frames, N=delta_step)
    return deltas


# delta proposed by Phuong
def delta_phuong(frames, win_len=0.025):
    padded = np.pad(frames, ((2, 2), (0, 0)), mode='edge')
    coeff = np.array([1, -0.8, 0, 0.8, -1]) # coefficient of each contexts

    delta = np.empty_like(frames)
    for t in range(len(frames)):
        delta[t] = np.dot(coeff, padded[t: t + 2 * 2 + 1]) / (win_len * 1000)
    return delta
    

def back_average(x, win_len=3, padding='edge'):
    padded = np.pad(x, (win_len - 1, 0), mode=padding)
    average = np.convolve(padded, np.ones(win_len) / win_len, mode='valid')
    return average


def average(x, win_len=3, padding='edge'):
    padded = np.pad(x, (0, win_len - 1), mode=padding)
    average = np.convolve(padded, np.ones(win_len) / win_len, mode='valid')
    return average


def back_averages(frames, win_len=3, padding='edge'):
    avg_frames = np.zeros_like(frames)
    for col in range(avg_frames.shape[1]):
        avg_frames[:, col] = back_average(frames[:, col], win_len, padding)
    return avg_frames


def averages(frames, win_len=3, padding='edge'):
    avg_frames = np.zeros_like(frames)
    for col in range(avg_frames.shape[1]):
        avg_frames[:, col] = average(frames[:, col], win_len, padding)
    return avg_frames


def apply_axis(array, func, axis=None, *args, **kwargs):
    if axis is None:
        axis = 0 if array.ndim == 1 else 1
    return np.apply_along_axis(func, axis, array, *args, **kwargs)


## visualizations ##


def frames_to_times(n, sample_rate=16000, win_step=0.01):
    return librosa.core.frames_to_time(np.arange(n), sr=sample_rate, hop_length=win_step * sample_rate, n_fft=None)


def get_formants(signal, sample_rate=16000, win_len=0.025, win_step=0.01, num_formant=5, max_formant=5500.0):
    sound = parselmouth.Sound(values=signal, sampling_frequency=sample_rate)
    pitch = sound.to_pitch()
    formant = sound.to_formant_burg(window_length=win_len, time_step=win_step, max_number_of_formants=num_formant,
                                    maximum_formant=max_formant)
    time = formant.ts()

    formants = []

    # F0
    array = np.empty(len(time))
    array[:] = np.NaN
    for i, t in enumerate(time):
        array[i] = pitch.get_value_at_time(time=t)

    formants.append(array)

    # F1, F2, ...
    for idx in range(1, num_formant):
        array = np.empty(len(time))
        array[:] = np.NaN

        for i, t in enumerate(time):
            array[i] = formant.get_value_at_time(formant_number=idx, time=t)

        formants.append(array)

    formants = np.array(formants)
    return formants, time


def plot_spectrogram(signal, sample_rate=16000, win_len=0.005, win_step=0.002, dynamic_range=70, x_axis=None,
                     x_label=None, y_axis=None, y_label=None, cmap=plt.cm.binary):
    # get spectrum
    frames = get_frames(signal=signal, sample_rate=sample_rate, win_len=win_len, win_step=win_step)
    spectrum = get_power_spectrum(frames)

    # convert spectrum to dB
    spec_in_db = power2decibel(spectrum.T)

    # scale x axis
    if x_axis == 'frame':
        x_label = 'Frame'
        x = np.linspace(0, spec_in_db.shape[1], spec_in_db.shape[1])
    else:
        x_label = 'Time (s)'
        x = frames_to_times(len(frames), sample_rate=sample_rate, win_step=win_step)

    # scale y axis
    if y_axis == 'bin':
        y_label = 'Frequency Bin'
        y = np.linspace(0, spec_in_db.shape[0], spec_in_db.shape[0])
    else:
        y_label = 'Frequency (Hz)'
        y = librosa.display.__coord_fft_hz(spec_in_db.shape[0] - 1, sr=sample_rate)

    # plot spectrogram
    ax = plt.pcolormesh(x, y, spec_in_db, vmin=spec_in_db.max() - dynamic_range, cmap=cmap)
    plt.xlabel(x_label)
    plt.ylabel(y_label)

    return ax, x, y


def plot_filterbanks(fbanks, freq, nfft=512, figsize=(8, 3)):
    fmin, fmax = freq.min(), freq.max()
    x = np.linspace(0, freq.max(), nfft // 2 + 1)

    fig, ax = plt.subplots(figsize=figsize)
    ax.plot(x, fbanks.T)
    ax.set_ylim((fbanks.min(), fbanks.max()))

    # frequency axis
    ax.grid(True)
    ax.set_xlim((fmin, fmax))
    ax.xaxis.set_ticks(freq)
    ax.set_ylabel('Weight')
    ax.set_xlabel('Frequency')

    # mel axis
    ax2 = ax.twiny()
    ax2.set_xlim((fmin, fmax))
    ax2.xaxis.set_ticks_position('top')
    ax2.xaxis.set_ticks(freq)
    ax2.xaxis.set_ticklabels([round(psf.hz2mel(f)) for f in freq])
    ax2.set_xlabel('Mel Frequency')
    return ax, x


def plot_waveform(signal, sample_rate=16000):
    x = np.linspace(0, len(signal) / sample_rate, num=len(signal))
    ax = plt.plot(x, signal)
    return ax, x
