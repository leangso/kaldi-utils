# This script will be called in make_sscf.sh

import os
import click
import kaldiio
import numpy as np
import subprocess
import sscf as sscf
import util as util
import normalize as norm


@click.command()
@click.option('--dataset_dir', type=str)
@click.option('--win_len', default=0.025)
@click.option('--win_step', default=0.01)

@click.option('--mfcc_enable', type=bool, default=False)

@click.option('--sscf_enable', type=bool, default=True)
@click.option('--sscf_smooth_step', type=int, default=3)
@click.option('--sscf_normalize_sscf0', type=bool, default=False)
@click.option('--sscf_include_sscf0', type=bool, default=False)

@click.option('--angle_enable', type=bool, default=False)
@click.option('--angle_delta_step', type=int, default=1)
@click.option('--angle_smooth_step', type=int, default=1)
@click.option('--angle_exclude_p01', type=bool, default=False)

@click.option('--speed_enable', type=bool, default=False)
@click.option('--speed_delta_step', type=int, default=1)
@click.option('--speed_smooth_step', type=int, default=1)
@click.option('--speed_exclude_p01', type=bool, default=False)

@click.option('--ats_enable', type=bool, default=False)
@click.option('--ats_delta_step', type=int, default=1)
@click.option('--ats_smooth_step', type=int, default=1)
@click.option('--ats_exclude_p01', type=bool, default=False)

@click.option('--pats_enable', type=bool, default=False)
@click.option('--pats_delta_step', type=int, default=1)
@click.option('--pats_smooth_step', type=int, default=1)
@click.option('--pats_exclude_p01', type=bool, default=False)

@click.option('--pats_ratio_enable', type=bool, default=False)
@click.option('--pats_ratio_delta_step', type=int, default=1)
@click.option('--pats_ratio_smooth_step', type=int, default=1)
@click.option('--pats_ratio_exclude_p01', type=bool, default=False)

@click.option('--ratio_enable', type=bool, default=False)
@click.option('--ratio_smooth_step', type=int, default=1)
@click.option('--ratio_exclude_p01', type=bool, default=False)

@click.option('--polar_enable', type=bool, default=False)
@click.option('--polar_exclude_p01', type=bool, default=False)
@click.option('--polar_exclude_radian', type=bool, default=False)
@click.option('--polar_speed_enable', type=bool, default=False)
@click.option('--polar_speed_exclude_raidan', type=bool, default=False)
@click.option('--polar_speed_smooth_step', type=int, default=1)
@click.option('--polar_angle_smooth_step', type=int, default=1)
@click.option('--polar_radian_smooth_step', type=int, default=1)

@click.option('--write_utt2num_frames', type=bool, default=True)
@click.option('--write_utt2dur', type=bool, default=True)
def main(dataset_dir,
        win_len,
        win_step,

        mfcc_enable,

        sscf_enable,
        sscf_smooth_step,
        sscf_normalize_sscf0,
        sscf_include_sscf0,

        angle_enable,
        angle_delta_step,
        angle_smooth_step,
        angle_exclude_p01,

        speed_enable,
        speed_delta_step,
        speed_smooth_step,
        speed_exclude_p01,

        ats_enable,
        ats_delta_step,
        ats_smooth_step,
        ats_exclude_p01,

        pats_enable,
        pats_delta_step,
        pats_smooth_step,
        pats_exclude_p01,

        pats_ratio_enable,
        pats_ratio_delta_step,
        pats_ratio_smooth_step,
        pats_ratio_exclude_p01,

        ratio_enable,
        ratio_smooth_step,
        ratio_exclude_p01,

        polar_enable,
        polar_exclude_p01,
        polar_exclude_radian,
        polar_speed_enable,
        polar_speed_exclude_raidan,
        polar_speed_smooth_step,
        polar_angle_smooth_step,
        polar_radian_smooth_step,

        write_utt2num_frames,
        write_utt2dur):

    dir_name = os.path.basename(dataset_dir)
    log_dir = os.path.join(dataset_dir, 'log')
    data_dir = os.path.join(dataset_dir, 'data')

    # find nj
    nj = len([x for x in os.listdir(log_dir) if 'wav' in x])
    if nj == 0:
        raise Exception('Wav scp files not found')

    if mfcc_enable:
        train_cmd = os.environ.get('train_cmd')
        cmd = f'steps/make_mfcc.sh --nj {nj} --cmd "{train_cmd}" {dataset_dir}'
        print(cmd)
        
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE)
        process.wait()
        if process.returncode != 0:
            raise Exception('Error computing MFCC: ', dataset_dir)

    for n in range(1, nj + 1):
        features = {}
        utt2num_frames = {}
        utt2durs = {}

        # read mfccs
        mfccs = {}
        if mfcc_enable:
            mfcc_scp = os.path.join(data_dir, 'raw_mfcc_%s.%s.scp' % (dir_name, n))
            reader = kaldiio.load_scp_sequential(mfcc_scp)
            for utt_id, data in reader:
                mfccs[utt_id] = data

        # compute sscf features
        wav_scp = os.path.join(log_dir, 'wav_%s.%s.scp' % (dir_name, n))
        reader = kaldiio.load_scp_sequential(wav_scp)
        for utt_id, (rate, data) in reader:
            feats = []

            frames = util.get_frames(data, win_len=win_len, win_step=win_step)
            sscfs = sscf.compute_sscfs(frames)

            if sscf_normalize_sscf0:
                sscfs[:, 0] = norm.semitone(sscfs[:, 0], 100)
    
            sscfs = util.averages(sscfs, win_len=sscf_smooth_step)

            if sscf_enable:
                feats.append(sscfs)
            elif sscf_include_sscf0:
                feats.append(sscfs[:, 0].reshape(-1, 1))

            if angle_enable:
                angles = sscf.compute_angles(sscfs, delta_step=angle_delta_step)
                if angle_smooth_step > 1:
                    angles = util.averages(angles, win_len=angle_smooth_step)
                if angle_exclude_p01:
                    angles = angles[:, 1:]
                feats.append(angles)

            if speed_enable:
                speeds = sscf.compute_speeds(sscfs, delta_step=speed_delta_step, win_len=win_len)
                if speed_smooth_step > 1:
                    speeds = util.averages(speeds, win_len=speed_smooth_step)
                if speed_exclude_p01:
                    speeds = speeds[:, 1:]
                feats.append(speeds)

            if ats_enable:
                ats = sscf.compute_ats(sscfs, delta_step=ats_delta_step, win_len=win_len)
                if ats_smooth_step > 1:
                    ats = util.averages(ats, win_len=ats_smooth_step)
                if ats_exclude_p01:
                    ats = ats[:, 1:]
                feats.append(ats)

            if pats_enable:
                pats = sscf.compute_pats(sscfs, delta_step=pats_delta_step, win_len=win_len)
                if pats_smooth_step > 1:
                    pats = util.averages(pats, win_len=pats_smooth_step)
                if pats_exclude_p01:
                    pats = pats[:, 1:]
                feats.append(pats)

            if pats_ratio_enable:
                pts_ratio = sscf.compute_pats_ratio(sscfs, delta_step=pats_ratio_delta_step)
                if pats_ratio_smooth_step > 1:
                    pts_ratio = util.averages(pts_ratio, win_len=pats_ratio_smooth_step)
                if pats_ratio_exclude_p01:
                    pts_ratio = pts_ratio[:, 1:]
                feats.append(pts_ratio)

            if ratio_enable:
                ratios = sscf.compute_ratios(sscfs)
                if ratio_smooth_step > 1:
                    ratios = util.averages(ratios, win_len=ratio_smooth_step)
                if ratio_exclude_p01:
                    ratios = ratios[:, 1:]
                feats.append(ratios)

            if polar_enable:
                radians, angles = sscf.compute_polars(sscfs)
                if polar_speed_enable:
                    speeds = sscf.compute_speeds(sscfs, delta_step=1, win_len=win_len)
                    if polar_speed_smooth_step > 1:
                        speeds = util.averages(speeds)

                    if not polar_speed_exclude_raidan:
                        radians *= speeds
                    angles *= speeds
                if polar_radian_smooth_step > 1:
                    radians = util.averages(radians)
                if polar_angle_smooth_step > 1:
                    angles = util.averages(angles)

                if polar_exclude_p01:
                    radians = radians[:, 1:]
                    angles = angles[:, 1:]
            
                if not polar_exclude_radian:
                    feats.append(radians)
                feats.append(angles)

            if mfcc_enable:
                mfccs_ = mfccs[utt_id]
                sscfs_ = np.hstack(feats)[:len(mfccs_)]
                features[utt_id] = np.hstack([mfccs_, sscfs_]) 
            else:
                features[utt_id] = np.hstack(feats)

            utt2num_frames[utt_id] = len(sscfs)
            utt2durs[utt_id] = len(data) / float(rate)

        # write ark and scp file
        ark_file = os.path.join(data_dir, 'raw_sscf_%s.%s.ark' % (dir_name, n))
        scp_file = os.path.join(data_dir, 'raw_sscf_%s.%s.scp' % (dir_name, n))
        kaldiio.save_ark(ark_file, features, scp=scp_file)

        # writer utt2num_frames file
        if write_utt2num_frames:
            utt2num_frames_file = os.path.join(log_dir, 'utt2num_frames.%s' % n)
            with open(utt2num_frames_file, 'w', encoding='utf-8') as writer:
                for utt_id, num_frames in utt2num_frames.items():
                    writer.write('%s %s\n' % (utt_id, str(num_frames)))

        # writer utt2durs file
        if write_utt2dur:
            utt2durs_file = os.path.join(log_dir, 'utt2dur.%s' % n)
            with open(utt2durs_file, 'w', encoding='utf-8') as writer:
                for utt_id, dur in utt2durs.items():
                    writer.write('%s %s\n' % (utt_id, str(dur)))


if __name__ == '__main__':
    main()

