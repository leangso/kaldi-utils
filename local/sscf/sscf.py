import numpy as np
import python_speech_features as psf

from python_speech_features import sigproc

import util as util
import pncc as pncc


# 6 filterbanks from report of Thomas
def get_filterbanks(sample_rate=16000, num_fft=512):
    # generate frequency bins
    freq = np.array([0, 303, 738, 1768, 3055, 4220, 5842, 8000])    # from Thomas's report
    bin = np.floor((num_fft + 1) * freq / sample_rate)

    # generate filterbanks
    fbank = np.zeros([6, num_fft // 2 + 1])    # 6 x 257
    for j in range(0, 6):
        for i in range(int(bin[j]), int(bin[j + 1])):
            fbank[j, i] = (i - bin[j]) / (bin[j + 1] - bin[j])
        for i in range(int(bin[j + 1]), int(bin[j + 2])):
            fbank[j, i] = (bin[j + 2] - i) / (bin[j + 2] - bin[j + 1])
    return fbank


# using filters from report of Thomas
def compute_sscfs(signal, sample_rate=16000, win_len=0.025, win_step=0.01, num_fft=512, preemph=0.97, win_func=np.hamming):
    # compute frames
    signal = sigproc.preemphasis(signal, preemph)
    frames = sigproc.framesig(sig=signal, frame_len=win_len * sample_rate, frame_step=win_step * sample_rate, winfunc=win_func)

    # compute power spectrum
    pspec = sigproc.powspec(frames, num_fft)
    pspec = np.where(pspec == 0, np.finfo(float).eps, pspec) # if things are all zeros we get problems

    # compute filterbank energies
    fb = get_filterbanks(sample_rate, num_fft)
    feat = np.dot(pspec, fb.T)  

    # compute centroids
    R = np.tile(np.linspace(1, sample_rate / 2, np.size(pspec, 1)), (np.size(pspec, 0), 1))
    sscfs = np.dot(pspec * R, fb.T) / feat
    return sscfs


def compute_sscfs_using_default_filters(signal, sample_rate=16000, win_len=0.025, win_step=0.01, num_filt=6,
                                        num_fft=512, low_freq=0, high_freq=None, preemph=0.97, win_func=np.hamming):
    # compute frames
    signal = sigproc.preemphasis(signal, preemph)
    frames = sigproc.framesig(sig=signal, frame_len=win_len * sample_rate, frame_step=win_step * sample_rate, winfunc=win_func)

    # compute power spectrum
    pspec = sigproc.powspec(frames, num_fft)
    pspec = np.where(pspec == 0, np.finfo(float).eps, pspec) # if things are all zeros we get problems


    # compute filterbank energies
    fb = psf.get_filterbanks(num_filt, num_fft, sample_rate, low_freq, high_freq)
    feat = np.dot(pspec, fb.T)

    # compute centroids
    R = np.tile(np.linspace(1, sample_rate / 2, np.size(pspec, 1)), (np.size(pspec, 0), 1))
    sscfs = np.dot(pspec * R, fb.T) / feat
    return sscfs


# def compute_sscfs_pn(frames, nfft=512):
#     # compute power spectrum
#     pspec = util.get_power_spectrum(frames, nfft)

#     # compute filterbank energies
#     fb = get_filterbanks(nfft)

#     feat = np.dot(pspec, fb.T)

#     #>>> start power normalization

#     power_stft_signal = feat
#     n_mels = 6

#     medium_time_power = pncc.medium_time_power_calculation(power_stft_signal)

#     #>>> start asymmetric noise supression with temporal masking
    
#     lower_envelope = pncc.asymmetric_lawpass_filtering(medium_time_power, 0.999, 0.5)

#     subtracted_lower_envelope = medium_time_power - lower_envelope

#     rectified_signal = pncc.halfwave_rectification(subtracted_lower_envelope)

#     floor_level = pncc.asymmetric_lawpass_filtering(rectified_signal)

#     temporal_masked_signal = pncc.temporal_masking(rectified_signal)

#     final_output = pncc.switch_excitation_or_non_excitation(temporal_masked_signal, floor_level, lower_envelope, medium_time_power)

#     #<<< end start asymmetric noise supression with temporal masking

#     spectral_weight_smoothing = pncc.weight_smoothing(final_output, medium_time_power, L=n_mels)

#     transfer_function = pncc.time_frequency_normalization(power_stft_signal, spectral_weight_smoothing)

#     normalized_power = pncc.mean_power_normalization(transfer_function, final_output, L=n_mels)

#     power_law_nonlinearity = pncc.power_function_nonlinearity(normalized_power)

#     feat = power_law_nonlinearity

#     #<<< end power normalization

#     # compute centroids
#     R = np.tile(np.linspace(1, 16000 / 2, np.size(pspec, 1)), (np.size(pspec, 0), 1))
#     sscfs = np.dot(pspec * R, fb.T) / feat
#     return sscfs


def compute_ratios(sscfs):
    num_frames, num_cols = sscfs.shape
    ratios = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1):
        ratios[:, col] = sscfs[:, col] / sscfs[:, col + 1]
    return ratios


def compute_angles(sscfs, delta_step=1, delta_func=util.delta):
    # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    angles = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        angles[:, col] = (180 / np.pi) * np.arctan2(deltas[:, col + 1], deltas[:, col])
    return angles


def compute_angles_360(sscfs, delta_step=1, delta_func=util.delta):
    # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    angles = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        results = (180 / np.pi) * np.arctan2(deltas[:, col + 1], deltas[:, col])
        results = np.where(results < 0, results + 360, results) # change angles to 360
        angles[:, col] = results
    return angles


def compute_angles_c1c2s(sscfs, delta_step=1, delta_func=util.delta):
     # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    c1s = np.zeros((num_frames, num_cols - 1))
    c2s = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        length = (deltas[:, col] ** 2 + deltas[:, col + 1] ** 2) ** 0.5
        length = np.where(length == 0, np.finfo(float).eps, length)
        c1 = deltas[:, col] / length
        c2 = deltas[:, col + 1] / length
        c1s[:, col] = c1
        c2s[:, col] = c2
    return c1s, c2s


def compute_delta_ratios(sscfs, delta_step=1, delta_func=util.delta):
    # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    ratios = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        denominators = np.where(deltas[:, col] == 0, np.finfo(float).eps, deltas[:, col])
        ratios[:, col] = deltas[:, col + 1] / denominators
    
    return ratios


def compute_speeds(sscfs, delta_step=1, delta_func=util.delta):
    # delta frames
    deltas = delta_func(sscfs, delta_step=delta_step)
    num_frames, num_cols = deltas.shape

    speeds = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1):
        speeds[:, col] = (deltas[:, col] ** 2 + deltas[:, col + 1] ** 2) ** 0.5
    return speeds


# def compute_speeds(sscfs, delta_step=1, delta_func=util.delta, win_len=0.025):
#     distances = compute_distances(sscfs, delta_step=delta_step, delta_func=delta_func)
#     # num_samples = np.full((distances.shape[0], 1), delta_step * win_len * 16000)
#     # return distances / num_samples
#     return distances


def compute_accelerations(sscfs, delta_step=1, delta_func=util.delta):
    speeds = compute_speeds(sscfs, delta_step=delta_step, delta_func=delta_func)
    acclerations = compute_speeds(speeds, delta_step=delta_step, delta_func=delta_func)
    return acclerations


def compute_ats(sscfs, delta_step=1, delta_func=util.delta, win_len=0.025):
    angles = compute_angles(sscfs, delta_step, delta_func)
    speeds = compute_speeds(sscfs, delta_step, delta_func, win_len)
    return angles * speeds


def compute_pats(sscfs, delta_step=1, delta_func=util.delta, win_len=0.025):
    pangles = compute_pangles(sscfs)
    speeds = compute_speeds(sscfs, delta_step, delta_func, win_len)
    return pangles * speeds


def compute_pangles(sscfs):
    num_frames, num_cols = sscfs.shape
    angles = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        angles[:, col] = (180 / np.pi) * np.arctan2(sscfs[:, col + 1], sscfs[:, col])
    return angles


def compute_pats_ratio(sscfs, delta_step=1):
    ratios = compute_ratios(sscfs)
    return compute_pangles(ratios) * compute_speeds(ratios, delta_step)


def compute_polars(sscfs):
    num_frames, num_cols = sscfs.shape
    radians = np.zeros((num_frames, num_cols - 1))
    angles = np.zeros((num_frames, num_cols - 1))
    for col in range(num_cols - 1): 
        radians[:, col] = (sscfs[:, col] ** 2 + sscfs[:, col + 1] ** 2) ** 0.5
        angles[:, col] = (180 / np.pi) * np.arctan2(sscfs[:, col + 1], sscfs[:, col])
    return radians, angles