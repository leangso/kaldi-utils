#!/usr/bin/env bash

nj=4
cmd=run.pl
write_utt2num_frames=true  # If true writes utt2num_frames.
write_utt2dur=true

win_len=0.025
win_step=0.01

mfcc_compute=false

sscf_compute=false
sscf_include=false  # if include in the features
sscf_smooth_step=3
sscf_normalize_sscf0=false
sscf_include_sscf0=false

angle_compute=false
angle_delta_step=1
angle_smooth_step=1
angle_exclude_p01=false

speed_compute=false
speed_delta_step=1
speed_smooth_step=1
speed_exclude_p01=false

ats_compute=false
ats_delta_step=1
ats_smooth_step=1
ats_exclude_p01=false

pats_compute=true
pats_delta_step=1
pats_smooth_step=1
pats_exclude_p01=false

pats_ratio_compute=false
pats_ratio_delta_step=1
pats_ratio_smooth_step=1
pats_ratio_exclude_p01=false

ratio_compute=false
ratio_smooth_step=1
ratio_exclude_p01=false

polar_compute=false
polar_exclude_p01=false
polar_exclude_radian=false
polar_speed_compute=false
polar_speed_exclude_raidan=false
polar_speed_smooth_step=1
polar_angle_smooth_step=1
polar_radian_smooth_step=1

# End configuration section.

echo "$0 $@"  # Print the command line for logging.

# load configuration
. ./conf/sscf.conf

# print configuration
printf "\nSSCF configurations\n"
cat ./conf/sscf.conf
printf "\n\n"

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# -lt 1 ] || [ $# -gt 3 ]; then
  cat >&2 <<EOF
Usage: $0 [options] <data-dir> ]
 e.g.: $0 data/train
Options:
  --nj <nj>                            # number of parallel jobs.
  --cmd <run.pl|queue.pl <queue opts>> # how to run jobs.
  --write-utt2num-frames <true|false>  # If true, write utt2num_frames file.
  --write-utt2dur <true|false>         # If true, write utt2dur file.
EOF
   exit 1;
fi

data=$1

logdir=$data/log
sscfdir=$data/data

# make $sscfdir an absolute pathname.
sscfdir=`perl -e '($dir,$pwd)= @ARGV; if($dir!~m:^/:) { $dir = "$pwd/$dir"; } print $dir; ' $sscfdir ${PWD}`

# use "name" as part of name of the archive.
name=`basename $data`

mkdir -p $sscfdir || exit 1;
mkdir -p $logdir || exit 1;

if [ -f $data/feats.scp ]; then
  mkdir -p $data/.backup
  echo "$0: moving $data/feats.scp to $data/.backup"
  mv $data/feats.scp $data/.backup
fi

scp=$data/wav.scp

required="$scp $sscf_config"

for f in $required; do
  if [ ! -f $f ]; then
    echo "$0: no such file $f"
    exit 1;
  fi
done

utils/validate_data_dir.sh --no-text --no-feats $data || exit 1;

split_scps=
for n in $(seq $nj); do
split_scps="$split_scps $logdir/wav_${name}.$n.scp"
done

utils/split_scp.pl $scp $split_scps || exit 1;

python3 local/sscf/compute_sscf_v2.py --dataset_dir $data \
  --win_len $win_len \
  --win_step $win_step \
  --mfcc_enable $mfcc_enable \
  --mfcc_num_cep $mfcc_num_cep \
  --mfcc_num_filt $mfcc_num_filt \
  --mfcc_append_energy $mfcc_append_energy \
  --sscf_enable $sscf_enable \
  --sscf_include $sscf_include \
  --sscf_smooth_step $sscf_smooth_step \
  --sscf_normalize_sscf0 $sscf_normalize_sscf0 \
  --sscf_include_sscf0 $sscf_include_sscf0 \
  --angle_enable $angle_enable \
  --angle_delta_step $angle_delta_step \
  --angle_smooth_step $angle_smooth_step \
  --angle_exclude_p01 $angle_exclude_p01 \
  --speed_enable $speed_enable \
  --speed_delta_step $speed_delta_step \
  --speed_smooth_step $speed_smooth_step \
  --speed_exclude_p01 $speed_exclude_p01 \
  --ats_enable $ats_enable \
  --ats_delta_step $ats_delta_step \
  --ats_smooth_step $ats_smooth_step \
  --ats_exclude_p01 $ats_exclude_p01 \
  --pats_enable $pats_enable \
  --pats_delta_step $pats_delta_step \
  --pats_smooth_step $pats_smooth_step \
  --pats_exclude_p01 $pats_exclude_p01 \
  --pats_ratio_enable $pats_ratio_enable \
  --pats_ratio_delta_step $pats_ratio_delta_step \
  --pats_ratio_smooth_step $pats_ratio_smooth_step \
  --pats_ratio_exclude_p01 $pats_ratio_exclude_p01 \
  --ratio_enable $ratio_enable \
  --ratio_smooth_step $ratio_smooth_step \
  --ratio_exclude_p01 $ratio_exclude_p01 \
  --polar_enable $polar_enable \
  --polar_exclude_p01 $polar_exclude_p01 \
  --polar_exclude_radian $polar_exclude_radian \
  --polar_speed_enable $polar_speed_enable \
  --polar_speed_exclude_raidan $polar_speed_exclude_raidan \
  --polar_speed_smooth_step $polar_speed_smooth_step \
  --polar_angle_smooth_step $polar_angle_smooth_step \
  --polar_radian_smooth_step $polar_radian_smooth_step \
  --write_utt2num_frames $write_utt2num_frames \
  --write_utt2dur $write_utt2dur || exit 1

# concatenate the .scp files together.
for n in $(seq $nj); do
  cat $sscfdir/raw_sscf_$name.$n.scp || exit 1
done > $data/feats.scp || exit 1

if $write_utt2num_frames; then
  for n in $(seq $nj); do
    cat $logdir/utt2num_frames.$n || exit 1
  done > $data/utt2num_frames || exit 1
fi

if $write_utt2dur; then
  for n in $(seq $nj); do
    cat $logdir/utt2dur.$n || exit 1
  done > $data/utt2dur || exit 1
fi

# store features size
echo `feat-to-dim scp:$data/feats.scp -` > $data/feats_size || exit 1

# store frame_shift and sscf_config along with features.
frame_shift=$(perl -ne 'if (/^winstep=(\d+)/) {
                          printf "%.3f", 0.001 * $1; exit; }' conf/sscf.conf)
echo ${frame_shift:-'0.01'} > $data/frame_shift
mkdir -p $data/conf && cp conf/sscf.conf $data/conf/sscf.conf || exit 1

rm $logdir/wav_${name}.*.scp $logdir/utt2num_frames.* $logdir/utt2dur.* 2>/dev/null

nf=$(wc -l < $data/feats.scp)
nu=$(wc -l < $data/utt2spk)
if [ $nf -ne $nu ]; then
  echo "$0: It seems not all of the feature files were successfully procesed" \
       "($nf != $nu); consider using utils/fix_data_dir.sh $data"
fi

if (( nf < nu - nu/20 )); then
  echo "$0: Less than 95% the features were successfully generated."\
       "Probably a serious error."
  exit 1
fi


echo "$0: Succeeded creating SSCF features for $name"
