# This script will be called in make_sscf.sh

import os
import click
import kaldiio
import numpy as np
import mfcc as mfcc
import sscf as sscf
import util as util
import normalize as norm


@click.command()
@click.option('--dataset_dir', type=str)
@click.option('--win_len', default=0.025)
@click.option('--win_step', default=0.01)

@click.option('--mfcc_enable', type=bool, default=True)
@click.option('--mfcc_include', type=bool, default=True)
@click.option('--mfcc_num_cep', type=int, default=6)
@click.option('--mfcc_num_filt', type=int, default=6)
@click.option('--mfcc_append_energy', type=bool, default=False)
@click.option('--mfcc_normalize_mfcc0', type=bool, default=False)
@click.option('--mfcc_normalize_mfcc0_method', type=str, default='st100')   # st100 | stavg
@click.option('--mfcc_append_delta', type=bool, default=False)

@click.option('--mfcc_polar_enable', type=bool, default=False)
@click.option('--mfcc_polar_exclude_planes', type=str, default="")
@click.option('--mfcc_polar_exclude_radian', type=bool, default=False)
@click.option('--mfcc_polar_radian_smooth_step', type=int, default=1)
@click.option('--mfcc_polar_angle_smooth_step', type=int, default=1)
@click.option('--mfcc_polar_append_delta', type=bool, default=False)

@click.option('--sscf_enable', type=bool, default=False)
@click.option('--sscf_filter', type=str, default='mfcc')    # phuong | mfcc
@click.option('--sscf_include', type=bool, default=False)
@click.option('--sscf_smooth_step', type=int, default=3)
@click.option('--sscf_normalize_sscf0', type=bool, default=False)
@click.option('--sscf_normalize_sscf0_method', type=str, default='st100')   # st100 | stavg
@click.option('--sscf_include_sscf0', type=bool, default=False)
@click.option('--sscf_append_delta', type=bool, default=False)

@click.option('--angle_enable', type=bool, default=False)
@click.option('--angle_delta_step', type=int, default=1)
@click.option('--angle_smooth_step', type=int, default=1)
@click.option('--angle_exclude_planes', type=str, default="")
@click.option('--angle_append_delta', type=bool, default=False)

@click.option('--speed_enable', type=bool, default=False)
@click.option('--speed_delta_step', type=int, default=1)
@click.option('--speed_smooth_step', type=int, default=1)
@click.option('--speed_exclude_planes', type=str, default="")
@click.option('--speed_append_delta', type=bool, default=False)

@click.option('--ats_enable', type=bool, default=False)
@click.option('--ats_delta_step', type=int, default=1)
@click.option('--ats_smooth_step', type=int, default=1)
@click.option('--ats_exclude_planes', type=str, default="")
@click.option('--ats_append_delta', type=bool, default=False)

@click.option('--pats_enable', type=bool, default=False)
@click.option('--pats_delta_step', type=int, default=1)
@click.option('--pats_smooth_step', type=int, default=1)
@click.option('--pats_exclude_planes', type=str, default="")
@click.option('--pats_append_delta', type=bool, default=False)

@click.option('--pats_ratio_enable', type=bool, default=False)
@click.option('--pats_ratio_delta_step', type=int, default=1)
@click.option('--pats_ratio_smooth_step', type=int, default=1)
@click.option('--pats_ratio_exclude_planes', type=str, default="")
@click.option('--pats_ratio_append_delta', type=bool, default=False)

@click.option('--ratio_enable', type=bool, default=False)
@click.option('--ratio_smooth_step', type=int, default=1)
@click.option('--ratio_exclude_planes', type=str, default="")
@click.option('--ratio_append_delta', type=bool, default=False)

@click.option('--polar_enable', type=bool, default=False)
@click.option('--polar_exclude_planes', type=str, default="")
@click.option('--polar_exclude_radian', type=bool, default=False)
@click.option('--polar_speed_enable', type=bool, default=False)
@click.option('--polar_speed_exclude_raidan', type=bool, default=False)
@click.option('--polar_speed_smooth_step', type=int, default=1)
@click.option('--polar_angle_smooth_step', type=int, default=1)
@click.option('--polar_radian_smooth_step', type=int, default=1)
@click.option('--polar_append_delta', type=bool, default=False)

@click.option('--write_utt2num_frames', type=bool, default=True)
@click.option('--write_utt2dur', type=bool, default=True)
def main(dataset_dir,
        win_len=0.025,
        win_step=0.01,

        mfcc_enable=True,
        mfcc_include=True,
        mfcc_num_cep=6,
        mfcc_num_filt=6,
        mfcc_append_energy=False,
        mfcc_normalize_mfcc0=False,
        mfcc_normalize_mfcc0_method='st100',   # st100 | stavg
        mfcc_append_delta=False,

        mfcc_polar_enable=False,
        mfcc_polar_exclude_planes="",
        mfcc_polar_exclude_radian=False,
        mfcc_polar_radian_smooth_step=1,
        mfcc_polar_angle_smooth_step=1,
        mfcc_polar_append_delta=False,

        sscf_enable=False,
        sscf_filter='mfcc',
        sscf_include=False,
        sscf_smooth_step=3,
        sscf_normalize_sscf0=False,
        sscf_normalize_sscf0_method='st100',    # st100 | stavg
        sscf_include_sscf0=False,
        sscf_append_delta=False,

        angle_enable=False,
        angle_delta_step=1,
        angle_smooth_step=1,
        angle_exclude_planes="",
        angle_append_delta=False,

        speed_enable=False,
        speed_delta_step=1,
        speed_smooth_step=1,
        speed_exclude_planes="",
        speed_append_delta=False,

        ats_enable=False,
        ats_delta_step=1,
        ats_smooth_step=1,
        ats_exclude_planes="",
        ats_append_delta=False,

        pats_enable=False,
        pats_delta_step=1,
        pats_smooth_step=1,
        pats_exclude_planes="",
        pats_append_delta=False,

        pats_ratio_enable=False,
        pats_ratio_delta_step=1,
        pats_ratio_smooth_step=1,
        pats_ratio_exclude_planes="",
        pats_ratio_append_delta=False,

        ratio_enable=False,
        ratio_smooth_step=1,
        ratio_exclude_planes="",
        ratio_append_delta=False,

        polar_enable=False,
        polar_exclude_planes="",
        polar_exclude_radian=False,
        polar_speed_enable=False,
        polar_speed_exclude_raidan=False,
        polar_speed_smooth_step=1,
        polar_angle_smooth_step=1,
        polar_radian_smooth_step=1,
        polar_append_delta=False,

        write_utt2num_frames=True,
        write_utt2dur=True):

    dir_name = os.path.basename(dataset_dir)
    log_dir = os.path.join(dataset_dir, 'log')
    data_dir = os.path.join(dataset_dir, 'data')

    planes = [0, 1, 2, 3, 4]    # SSCF planes, 0: SSCF0/SSCF1

    # find nj
    nj = len([x for x in os.listdir(log_dir) if 'wav' in x])
    if nj == 0:
        raise Exception('Wav scp files not found')

    for n in range(1, nj + 1):
        features = {}
        utt2num_frames = {}
        utt2durs = {}

        # compute features
        wav_scp = os.path.join(log_dir, 'wav_%s.%s.scp' % (dir_name, n))
        reader = kaldiio.load_scp_sequential(wav_scp)
        for utt_id, (rate, signal) in reader:
            feats = []
            Ds = []
            DDs = []

            # mfcc
            if mfcc_enable:
                mfccs = mfcc.compute_mfccs(signal, sample_rate=rate, win_len=win_len, win_step=win_step, num_cep=mfcc_num_cep,
                                           num_filt=mfcc_num_filt, append_energy=mfcc_append_energy)

                if mfcc_normalize_mfcc0:
                    if mfcc_normalize_mfcc0_method == 'st100':
                        mfccs[:, 0] = norm.semitone(mfccs[:, 0], 100)
                    elif mfcc_normalize_mfcc0_method == 'stavg':
                        mfccs[:, 0] = norm.semitone_avg(mfccs[:, 0])
                    else:
                        raise Exception('Unsupport normalization method: ', mfcc_normalize_mfcc0_method)
        
                if mfcc_include:
                    feats.append(mfccs)
                    
                    if mfcc_append_delta:
                        D, DD = compute_delta(mfccs)
                        Ds.append(D)
                        DDs.append(DD)

                utt2num_frames[utt_id] = len(mfccs)
                utt2durs[utt_id] = len(signal) / float(rate)

                # polar
                if mfcc_polar_enable:
                    radians, angles = sscf.compute_polars(mfccs)

                    if mfcc_polar_radian_smooth_step > 1:
                        radians = util.averages(radians, win_len=mfcc_polar_radian_smooth_step)
                    if mfcc_polar_angle_smooth_step > 1:
                        angles = util.averages(angles, win_len=mfcc_polar_angle_smooth_step)

                    if mfcc_polar_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(mfcc_polar_exclude_planes)))
                        radians = radians[:, include_planes]
                        angles = angles[:, include_planes]
                
                    if not mfcc_polar_exclude_radian:
                        feats.append(radians)
        
                    feats.append(angles)

                    if mfcc_polar_append_delta:
                        if not mfcc_polar_exclude_radian:
                            D, DD = compute_delta(radians)
                            Ds.append(D)
                            DDs.append(DD)

                        D, DD = compute_delta(angles)
                        Ds.append(D)
                        DDs.append(DD)

            # sscf
            if sscf_enable:
                if sscf_filter == 'mfcc':
                    sscfs = sscf.compute_sscfs_using_default_filters(signal, sample_rate=rate, win_len=win_len, win_step=win_step)
                elif sscf_filter == 'phuong':
                    sscfs = sscf.compute_sscfs(signal, sample_rate=rate, win_len=win_len, win_step=win_step)
                else:
                    raise Exception('Unsupport filter: ', sscf_filter)

                if sscf_smooth_step > 1:
                    sscfs = util.averages(sscfs, win_len=sscf_smooth_step)
                if sscf_normalize_sscf0:
                    if sscf_normalize_sscf0_method == 'st100':
                        sscfs[:, 0] = norm.semitone(sscfs[:, 0], 100)
                    elif sscf_normalize_sscf0_method == 'stavg':
                        sscfs[:, 0] = norm.semitone_avg(sscfs[:, 0])
                    else:
                        raise Exception('Unsupport normalization method: ', sscf_normalize_sscf0_method)

                if sscf_include:
                    feats.append(sscfs)

                    if sscf_append_delta:
                        D, DD = compute_delta(sscfs)
                        Ds.append(D)
                        DDs.append(DD)

                elif sscf_include_sscf0:
                    feats.append(sscfs[:, 0].reshape(-1, 1))

                    if sscf_append_delta:
                        D, DD = compute_delta(sscfs[:, 0].reshape(-1, 1))
                        Ds.append(D.reshape(-1, 1))
                        DDs.append(DD.reshape(-1, 1))

                # angle
                if angle_enable:
                    angles = sscf.compute_angles(sscfs, delta_step=angle_delta_step)
                    if angle_smooth_step > 1:
                        angles = util.averages(angles, win_len=angle_smooth_step)
                    if angle_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(angle_exclude_planes)))
                        angles = angles[:, include_planes]
                    feats.append(angles)

                    if angle_append_delta:
                        D, DD = compute_delta(angles)
                        Ds.append(D)
                        DDs.append(DD)

                    # c1s, c2s = sscf.compute_angles_c1c2s(sscfs)

                    # if angle_smooth_step > 1:
                    #     c1s = util.averages(c1s, win_len=angle_smooth_step)
                    #     c2s = util.averages(c1s, win_len=angle_smooth_step)

                    # if angle_exclude_planes != '':
                    #     include_planes = list(set(planes) - set(eval(angle_exclude_planes)))
                    #     c1s = c1s[:, include_planes]
                    #     c2s = c2s[:, include_planes]

                    # feats.append(c1s)
                    # feats.append(c2s)

                # speed
                if speed_enable:
                    speeds = sscf.compute_speeds(sscfs, delta_step=speed_delta_step, win_len=win_len)
                    if speed_smooth_step > 1:
                        speeds = util.averages(speeds, win_len=speed_smooth_step)
                    if speed_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(speed_exclude_planes)))
                        speeds = speeds[:, include_planes]
                    feats.append(speeds)

                    if speed_append_delta:
                        D, DD = compute_delta(speeds)
                        Ds.append(D)
                        DDs.append(DD)

                # ats
                if ats_enable:
                    ats = sscf.compute_ats(sscfs, delta_step=ats_delta_step, win_len=win_len)
                    if ats_smooth_step > 1:
                        ats = util.averages(ats, win_len=ats_smooth_step)
                    if ats_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(ats_exclude_planes)))
                        ats = ats[:, include_planes]
                    feats.append(ats)

                    if ats_append_delta:
                        D, DD = compute_delta(ats)
                        Ds.append(D)
                        DDs.append(DD)

                # pats
                if pats_enable:
                    pats = sscf.compute_pats(sscfs, delta_step=pats_delta_step, win_len=win_len)
                    if pats_smooth_step > 1:
                        pats = util.averages(pats, win_len=pats_smooth_step)
                    if pats_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(pats_exclude_planes)))
                        pats = pats[:, include_planes]
                    feats.append(pats)

                    if pats_append_delta:
                        D, DD = compute_delta(pats)
                        Ds.append(D)
                        DDs.append(DD)

                # pats_ratio
                if pats_ratio_enable:
                    pts_ratio = sscf.compute_pats_ratio(sscfs, delta_step=pats_ratio_delta_step)
                    if pats_ratio_smooth_step > 1:
                        pts_ratio = util.averages(pts_ratio, win_len=pats_ratio_smooth_step)
                    if pats_ratio_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(pats_ratio_exclude_planes)))
                        pts_ratio = pts_ratio[:, include_planes]
                    feats.append(pts_ratio)

                    if pats_ratio_append_delta:
                        D, DD = compute_delta(pts_ratio)
                        Ds.append(D)
                        DDs.append(DD)

                # ratio
                if ratio_enable:
                    ratios = sscf.compute_ratios(sscfs)
                    if ratio_smooth_step > 1:
                        ratios = util.averages(ratios, win_len=ratio_smooth_step)
                    if ratio_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(ratio_exclude_planes)))
                        ratios = ratios[:, include_planes]
                    feats.append(ratios)

                    if ratio_append_delta:
                        D, DD = compute_delta(ratios)
                        Ds.append(D)
                        DDs.append(DD)
                
                # polar
                if polar_enable:
                    radians, angles = sscf.compute_polars(sscfs)
                    if polar_speed_enable:
                        speeds = sscf.compute_speeds(sscfs, delta_step=1, win_len=win_len)
                        if polar_speed_smooth_step > 1:
                            speeds = util.averages(speeds, win_len=polar_speed_smooth_step)

                        if not polar_speed_exclude_raidan:
                            radians *= speeds
                        angles *= speeds
                        
                    if polar_radian_smooth_step > 1:
                        radians = util.averages(radians, win_len=polar_radian_smooth_step)
                    if polar_angle_smooth_step > 1:
                        angles = util.averages(angles, win_len=polar_angle_smooth_step)

                    if polar_exclude_planes != '':
                        include_planes = list(set(planes) - set(eval(polar_exclude_planes)))
                        radians = radians[:, include_planes]
                        angles = angles[:, include_planes]
                
                    if not polar_exclude_radian:
                        feats.append(radians)
        
                    feats.append(angles)

                    if polar_append_delta:
                        if not polar_exclude_radian:
                            D, DD = compute_delta(radians)
                            Ds.append(D)
                            DDs.append(DD)

                        D, DD = compute_delta(angles)
                        Ds.append(D)
                        DDs.append(DD)

                if not mfcc_enable:
                    utt2num_frames[utt_id] = len(sscfs)
                    utt2durs[utt_id] = len(signal) / float(rate)

            combine_feats = np.hstack(feats)
            if len(Ds) > 0:
                combine_feats = np.column_stack([combine_feats, np.hstack(Ds), np.hstack(DDs)]) 
            features[utt_id] = combine_feats


        # write ark and scp file
        ark_file = os.path.join(data_dir, 'raw_sscf_%s.%s.ark' % (dir_name, n))
        scp_file = os.path.join(data_dir, 'raw_sscf_%s.%s.scp' % (dir_name, n))
        kaldiio.save_ark(ark_file, features, scp=scp_file)

        # writer utt2num_frames file
        if write_utt2num_frames:
            utt2num_frames_file = os.path.join(log_dir, 'utt2num_frames.%s' % n)
            with open(utt2num_frames_file, 'w', encoding='utf-8') as writer:
                for utt_id, num_frames in utt2num_frames.items():
                    writer.write('%s %s\n' % (utt_id, str(num_frames)))

        # writer utt2durs file
        if write_utt2dur:
            utt2durs_file = os.path.join(log_dir, 'utt2dur.%s' % n)
            with open(utt2durs_file, 'w', encoding='utf-8') as writer:
                for utt_id, dur in utt2durs.items():
                    writer.write('%s %s\n' % (utt_id, str(dur)))


def compute_delta(feat, N=2):
    D = mfcc.delta(feat, N)
    DD = mfcc.delta(D, N)
    return D, DD


if __name__ == '__main__':
    main()


