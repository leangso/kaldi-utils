#!/usr/bin/env bash

# ats => angle x speed
# pats => polar angles x speed
# exclude planes => [0, 1, 2, 3, 4]

nj=4
cmd=run.pl
write_utt2num_frames=true  # If true writes utt2num_frames.
write_utt2dur=true

win_len=0.025
win_step=0.01

mfcc_enable=true
mfcc_include=true  # if include in the features
mfcc_num_cep=6
mfcc_num_filt=6
mfcc_append_energy=false
mfcc_normalize_mfcc0=false
mfcc_normalize_mfcc0_method=st100   # st100 | stavg
mfcc_append_delta=false

mfcc_polar_enable=false
mfcc_polar_exclude_planes=""
mfcc_polar_exclude_radian=false
mfcc_polar_radian_smooth_step=1
mfcc_polar_angle_smooth_step=1
mfcc_polar_append_delta=false

sscf_enable=false
sscf_filter=mfcc    # phuong|mfcc  
sscf_include=false  # if include in the features
sscf_smooth_step=3
sscf_normalize_sscf0=false
sscf_normalize_sscf0_method=st100   # st100 | stavg
sscf_include_sscf0=false
sscf_append_delta=false

angle_enable=false
angle_delta_step=1
angle_smooth_step=1
angle_exclude_planes=""
angle_append_delta=false

speed_enable=false
speed_delta_step=1
speed_smooth_step=1
speed_exclude_planes=""
speed_append_delta=false

ats_enable=false
ats_delta_step=1
ats_smooth_step=1
ats_exclude_planes=""
ats_append_delta=false

pats_enable=false
pats_delta_step=1
pats_smooth_step=1
pats_exclude_planes=""
pats_append_delta=false

pats_ratio_enable=false
pats_ratio_delta_step=1
pats_ratio_smooth_step=1
pats_ratio_exclude_planes=""
pats_ratio_append_delta=false

ratio_enable=false
ratio_smooth_step=1
ratio_exclude_planes=""
ratio_append_delta=false

polar_enable=false
polar_exclude_planes=""
polar_exclude_radian=false
polar_speed_enable=false
polar_speed_exclude_raidan=false
polar_speed_smooth_step=1
polar_angle_smooth_step=1
polar_radian_smooth_step=1
polar_append_delta=false

# End configuration section.

echo "$0 $@"  # Print the command line for logging.

# load configuration
. ./conf/sscf.conf

# print configuration
printf "\nSSCF configurations\n"
cat ./conf/sscf.conf
printf "\n\n"

if [ -f path.sh ]; then . ./path.sh; fi
. parse_options.sh || exit 1;

if [ $# -lt 1 ] || [ $# -gt 3 ]; then
  cat >&2 <<EOF
Usage: $0 [options] <data-dir> ]
 e.g.: $0 data/train
Options:
  --nj <nj>                            # number of parallel jobs.
  --cmd <run.pl|queue.pl <queue opts>> # how to run jobs.
  --write-utt2num-frames <true|false>  # If true, write utt2num_frames file.
  --write-utt2dur <true|false>         # If true, write utt2dur file.
EOF
   exit 1;
fi

data=$1

logdir=$data/log
sscfdir=$data/data

# make $sscfdir an absolute pathname.
sscfdir=`perl -e '($dir,$pwd)= @ARGV; if($dir!~m:^/:) { $dir = "$pwd/$dir"; } print $dir; ' $sscfdir ${PWD}`

# use "name" as part of name of the archive.
name=`basename $data`

mkdir -p $sscfdir || exit 1;
mkdir -p $logdir || exit 1;

if [ -f $data/feats.scp ]; then
  mkdir -p $data/.backup
  echo "$0: moving $data/feats.scp to $data/.backup"
  mv $data/feats.scp $data/.backup
fi

scp=$data/wav.scp

required="$scp $sscf_config"

for f in $required; do
  if [ ! -f $f ]; then
    echo "$0: no such file $f"
    exit 1;
  fi
done

utils/validate_data_dir.sh --no-text --no-feats $data || exit 1;

split_scps=
for n in $(seq $nj); do
split_scps="$split_scps $logdir/wav_${name}.$n.scp"
done

utils/split_scp.pl $scp $split_scps || exit 1;

python3 local/sscf/compute_sscf_v2.py --dataset_dir $data \
  --win_len $win_len \
  --win_step $win_step \
  --mfcc_enable $mfcc_enable \
  --mfcc_include $mfcc_include \
  --mfcc_num_cep $mfcc_num_cep \
  --mfcc_num_filt $mfcc_num_filt \
  --mfcc_append_energy $mfcc_append_energy \
  --mfcc_normalize_mfcc0 $mfcc_normalize_mfcc0 \
  --mfcc_normalize_mfcc0_method $mfcc_normalize_mfcc0_method \
  --mfcc_append_delta $mfcc_append_delta \
  --mfcc_polar_enable $mfcc_polar_enable \
  --mfcc_polar_exclude_planes "$mfcc_polar_exclude_planes" \
  --mfcc_polar_exclude_radian $mfcc_polar_exclude_radian \
  --mfcc_polar_radian_smooth_step $mfcc_polar_radian_smooth_step \
  --mfcc_polar_angle_smooth_step $mfcc_polar_angle_smooth_step \
  --mfcc_polar_append_delta $mfcc_polar_append_delta \
  --sscf_enable $sscf_enable \
  --sscf_filter $sscf_filter \
  --sscf_include $sscf_include \
  --sscf_smooth_step $sscf_smooth_step \
  --sscf_normalize_sscf0 $sscf_normalize_sscf0 \
  --sscf_normalize_sscf0_method $sscf_normalize_sscf0_method \
  --sscf_include_sscf0 $sscf_include_sscf0 \
  --sscf_append_delta $sscf_append_delta \
  --angle_enable $angle_enable \
  --angle_delta_step $angle_delta_step \
  --angle_smooth_step $angle_smooth_step \
  --angle_exclude_planes "$angle_exclude_planes" \
  --angle_append_delta $angle_append_delta \
  --speed_enable $speed_enable \
  --speed_delta_step $speed_delta_step \
  --speed_smooth_step $speed_smooth_step \
  --speed_exclude_planes "$speed_exclude_planes" \
  --speed_append_delta $speed_append_delta \
  --ats_enable $ats_enable \
  --ats_delta_step $ats_delta_step \
  --ats_smooth_step $ats_smooth_step \
  --ats_exclude_planes "$ats_exclude_planes" \
  --ats_append_delta $ats_append_delta \
  --pats_enable $pats_enable \
  --pats_delta_step $pats_delta_step \
  --pats_smooth_step $pats_smooth_step \
  --pats_exclude_planes "$pats_exclude_planes" \
  --pats_append_delta $pats_append_delta \
  --pats_ratio_enable $pats_ratio_enable \
  --pats_ratio_delta_step $pats_ratio_delta_step \
  --pats_ratio_smooth_step $pats_ratio_smooth_step \
  --pats_ratio_exclude_planes "$pats_ratio_exclude_planes" \
  --pats_ratio_append_delta $pats_ratio_append_delta \
  --ratio_enable $ratio_enable \
  --ratio_smooth_step $ratio_smooth_step \
  --ratio_exclude_planes "$ratio_exclude_planes" \
  --ratio_append_delta $ratio_append_delta \
  --polar_enable $polar_enable \
  --polar_exclude_planes "$polar_exclude_planes" \
  --polar_exclude_radian $polar_exclude_radian \
  --polar_speed_enable $polar_speed_enable \
  --polar_speed_exclude_raidan $polar_speed_exclude_raidan \
  --polar_speed_smooth_step $polar_speed_smooth_step \
  --polar_angle_smooth_step $polar_angle_smooth_step \
  --polar_radian_smooth_step $polar_radian_smooth_step \
  --polar_append_delta $polar_append_delta \
  --write_utt2num_frames $write_utt2num_frames \
  --write_utt2dur $write_utt2dur || exit 1

# concatenate the .scp files together.
for n in $(seq $nj); do
  cat $sscfdir/raw_sscf_$name.$n.scp || exit 1
done > $data/feats.scp || exit 1

if $write_utt2num_frames; then
  for n in $(seq $nj); do
    cat $logdir/utt2num_frames.$n || exit 1
  done > $data/utt2num_frames || exit 1
fi

if $write_utt2dur; then
  for n in $(seq $nj); do
    cat $logdir/utt2dur.$n || exit 1
  done > $data/utt2dur || exit 1
fi

# store features size
echo `feat-to-dim scp:$data/feats.scp -` > $data/feats_size || exit 1

# store frame_shift and sscf_config along with features.
frame_shift=$(perl -ne 'if (/^winstep=(\d+)/) {
                          printf "%.3f", 0.001 * $1; exit; }' conf/sscf.conf)
echo ${frame_shift:-'0.01'} > $data/frame_shift
mkdir -p $data/conf && cp conf/sscf.conf $data/conf/sscf.conf || exit 1

# rm $logdir/wav_${name}.*.scp $logdir/utt2num_frames.* $logdir/utt2dur.* 2>/dev/null

nf=$(wc -l < $data/feats.scp)
nu=$(wc -l < $data/utt2spk)
if [ $nf -ne $nu ]; then
  echo "$0: It seems not all of the feature files were successfully procesed" \
       "($nf != $nu); consider using utils/fix_data_dir.sh $data"
fi

if (( nf < nu - nu/20 )); then
  echo "$0: Less than 95% the features were successfully generated."\
       "Probably a serious error."
  exit 1
fi


echo "$0: Succeeded creating SSCF features for $name"
