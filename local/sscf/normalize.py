import numpy as np

from scipy import signal
import util as util


def zscore(x):
    return (x - x.mean()) / x.std()


def semitone(x, ref=100):
    x = np.where(x == 0, np.finfo(float).eps, x)    # prevent zero for log
    return (12 / np.log10(2)) * np.log10(x / ref)


def semitone_avg(x):
    return semitone(x, np.mean(x))


def over_max_sscf0(x):
    return x / np.max(x)


def diff_sscf(sscf, sscf_mean=None, sscf_def=100):
    if sscf_mean is None:
        sscf_mean = np.mean(sscf)
    return sscf - (sscf_mean - sscf_def)


def diff_sscfs(sscfs, sscf0_def=100):
    sscf0_mean = np.mean(sscfs[:, 0])
    return util.apply_axis(sscfs, diff_sscf, axis=0, sscf_mean=sscf0_mean, sscf_def=sscf0_def)


def savgol(arr, window_length, polyorder):
    return signal.savgol_filter(arr, window_length=window_length, polyorder=polyorder)
