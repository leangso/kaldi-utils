import re
from collections import Counter
from operator import itemgetter
import argparse


def generate_vocab(corpus_file, vocab_file):
    vocab_counter = Counter()
    print('Read corpus %s' % corpus_file)
    with open(corpus_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            words = line.strip().split()
            vocab_counter.update(words)

    # write vocab
    print('Write vocab %s ' % vocab_file)
    with open(vocab_file, 'w', encoding='utf-8') as writer:
        lexicon = sorted(vocab_counter.items(), key=itemgetter(0))
        for word, n in lexicon:
            if word in ['<s>', '</s>']:
                continue
            writer.write('%s\n' % word)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--corpus_file', type=str)
    parser.add_argument('--output_file', type=str)
    
    args = parser.parse_args()

    generate_vocab(args.corpus_file, args.output_file)