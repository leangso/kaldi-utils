#!/usr/bin/env bash

train_set=
test_set=
vocab_file=
output_dir=

order=3
discount="-wbdiscount"
prune="-prune 1e-8"

. ./utils/parse_options.sh

train_set=($train_set)
test_set=($test_set)

dir=`dirname "$0"`

# check parameters
[ -z $train_set ] && echo >&2 "base_set is undefined" && exit 1
[ -z $test_set ] && echo >&2 "test_set is undefined" && exit 1
[ -z $output_dir ] && echo >&2 "output_dir is undefined" && exit 1

printf "\n--> Build Train Corpus \n"

# merge text corpus
train_corpus=$output_dir/train.corpus
for data_set in ${train_set[@]}; do
  printf "\nMerging $data_set to $train_corpus..."
  cat $data_set >> $train_corpus
done

# extract vocab
train_vocab=$output_dir/train.vocab
if [ -z $vocab_file ]; then
  printf "\nExtracting $train_vocab from $train_corpus...\n"
  python3 $dir/generate_vocab.py --corpus_file $train_corpus --output_file $train_vocab
else
  printf "\nPreparing $train_vocab from $vocab_file...\n"
  cat $vocab_file > $train_vocab
fi

printf "\n--> Train Model \n"

# build lm
model=$output_dir/model.lm
printf "\nBuilding $model from $train_corpus...\n"
ngram-count -order $order -text $train_corpus $discount -vocab $train_vocab -lm $model || exit 1

# prune lm
if [ ! -z "$prune" ]; then
  printf "\n--> Prune Model \n"
  prune_lm=$output_dir/model.lm.prune

  printf "\nPruning $model...: $prune\n"
  ngram -order $order -lm $model $prune -write-lm $prune_lm || exit 1
  rm -rf $model && mv $prune_lm $model
fi

printf "\n--> Test Model \n"

# compute perplexity from test set
for i in "${!test_set[@]}"; do
  data_set=${test_set[$i]}
  test_ppl=$output_dir/test_${i}.ppl
  
  printf "\nComputing $test_ppl of $data_set...\n"
  ngram -lm $model -ppl $data_set -debug 2 > $test_ppl || exit 1
  tail -n 2 $test_ppl
done