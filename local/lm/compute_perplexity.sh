#!/usr/bin/env bash

# parameters
model=$1
file_in=$2
file_out=$3

[ -z $model ] && echo >&2 "model is undefined" && exit 1
[ -z $file_in ] && echo >&2 "file_in is undefined" && exit 1
[ -z $file_out ] && echo >&2 "file_out is undefined" && exit 1

# compute perplexity
ngram -lm $model -ppl $file_in -debug 2 > $file_out
tail -n 2 $file_out