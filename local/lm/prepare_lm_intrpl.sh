#!/usr/bin/env bash

# brew install coreutils for shuf

base_set=
generic_set=
dev_set=      # for interpolation
test_set=
output_dir=

order=3
heldout_sent=10000
discount="-wbdiscount" # -gt1min 1 -gt2min 1 -gt3min 1 -wbdiscount -interpolate
prune="-prune 1e-8"

. ./utils/parse_options.sh

base_set=($base_set)
generic_set=($generic_set)
test_set=($test_set)

dir=`dirname "$0"`

# check parameters
[ -z $base_set ] && echo >&2 "base_set is undefined" && exit 1
[ -z $generic_set ] && echo >&2 "generic_set is undefined" && exit 1
[ -z $test_set ] && echo >&2 "test_set is undefined" && exit 1
[ -z $output_dir ] && echo >&2 "output_dir is undefined" && exit 1

######### BASE #########

printf "\n--> Building Base Model\n"
# merge text corpus
base_corpus=$output_dir/base.corpus
for data_set in ${base_set[@]}; do
  printf "\nMerging $data_set to $train_corpus ..."
  cat $data_set >> $base_corpus
done

# extract vocab
base_vocab=$output_dir/base.vocab
printf "\nExtracting $base_vocab from $base_corpus ...\n"
python3 $dir/generate_vocab.py --corpus_file $base_corpus --output_file $base_vocab

# build lm
base_lm=$output_dir/base.lm
printf "\nBuilding $base_lm from $base_set ...\n"
ngram-count -order $order -text $base_corpus $discount -vocab $base_vocab -lm $base_lm

# compute perplexity from test set
for i in "${!test_set[@]}"; do
  data_set=${test_set[$i]}
  test_ppl=$output_dir/base_${i}.ppl
  
  printf "\nComputing $test_ppl of $data_set ...\n"
  ngram -lm $base_lm -ppl $data_set -debug 2 > $test_ppl || exit 1
  tail -n 2 $test_ppl
done

######### GENERIC #########

printf "\n--> Building Generic Model\n"

if [ -z "$dev_set" ]; then
  # merge text corpus
  generic_tmp_corpus=$output_dir/generic.corpus.tmp
  for data_set in ${generic_set[@]}; do
    printf "\nShuffling and merging $data_set to $generic_tmp_corpus ..."
    shuf $data_set >> $generic_tmp_corpus
  done
  
  dev_set=$output_dir/dev.corpus
  base_corpus=$output_dir/generic.corpus
  
  printf "\nUsing $heldout_sent sentences as dev set: $dev_set ...\n"
  cat $generic_tmp_corpus | head -$heldout_sent > $dev_set
  cat $generic_tmp_corpus | tail -n +$heldout_sent > $base_corpus
  
  rm -rf $generic_tmp_corpus
else
  # merge text corpus
  generic_corpus=$output_dir/generic.corpus
  for data_set in ${generic_set[@]}; do
    printf "\nMerging $data_set to $generic_corpus ..."
    cat $data_set >> $generic_corpus
  done
fi

# extract vocab
generic_vocab=$output_dir/generic.vocab
printf "\nExtracting $generic_vocab from $generic_set ...\n"
python3 $dir/generate_vocab.py --corpus_file $generic_corpus --output_file $generic_vocab

# build lm
generic_lm=$output_dir/generic.lm
printf "\nBuilding $generic_lm from $generic_set ...\n"
ngram-count -order $order -text $generic_set $discount -vocab $generic_vocab -lm $generic_lm || exit 1

# compute perplexity from test sets
for i in "${!test_set[@]}"; do
  data_set=${test_set[$i]}
  test_ppl=$output_dir/generic_${i}.ppl
  
  printf "\nComputing $test_ppl of $data_set ...\n"
  ngram -lm $generic_lm -ppl $data_set -debug 2 > $test_ppl || exit 1
  tail -n 2 $test_ppl
done

######### Interpolate Models #########

printf "\n--> Interpolate Models\n"

# extract interpolate vocab
interpolate_vocab=$output_dir/interpolate.vocab
printf "\nBuilding $interpolate_vocab from $base_vocab, $generic_vocab ...\n"
cat $base_vocab $generic_vocab | sort -u > $interpolate_vocab || exit 1

# compute best perplexity
best_interpolate_ppl=$output_dir/best_interpolate.ppl
printf "\nComputing $best_interpolate_ppl using $train_ppl $generic_ppl ...\n"
compute-best-mix $output_dir/base_0.ppl $output_dir/generic_0.ppl > $best_interpolate_ppl || exit 1

# build interpolate model
interpolate_lm=$output_dir/interpolate.lm
lambda=($(head -1 $best_interpolate_ppl | grep -o -E '[0-9]+.?[0-9]+'))
printf "\nBest lambda (${lambda[1]}, ${lambda[2]})\n"

printf "\nBuilding $interpolate_lm ...\n"
ngram -order $order -unk -vocab $interpolate_vocab \
  -lm $base_lm -lambda ${lambda[1]} \
  -mix-lm $generic_lm \
  -write-lm $interpolate_lm || exit 1

# prune interpolate lm
if [ ! -z "$prune" ]; then
  prune_interpolate_lm=$output_dir/interpolate.lm.prune
  printf "\nPruning $prune_interpolate_lm with $prune...\n"
  ngram -lm $interpolate_lm $prune -write-lm $prune_interpolate_lm || exit 1

  interpolate_lm=$prune_interpolate_lm
fi

# compute perplexity from test sets
for i in "${!test_set[@]}"; do
  data_set=${test_set[$i]}
  test_ppl=$output_dir/interpolate_${i}.ppl
  
  printf "\nComputing $test_ppl of $data_set ...\n"
  ngram -lm $interpolate_lm -ppl $data_set -debug 2 > $test_ppl || exit 1
  tail -n 2 $test_ppl
done
