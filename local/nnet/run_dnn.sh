#!/bin/bash

# the script is from https://github.com/sarahjuan/iban
#   iban/kaldi-scripts/local/run_dnn.sh

# Copyright 2012-2014  Brno University of Technology (Author: Karel Vesely)
# Apache 2.0

# This example script trains a DNN on top of fMLLR features. 
# The training is done in 3 stages,
#
# 1) RBM pre-training:
#    in this unsupervised stage we train stack of RBMs, 
#    a good starting point for frame cross-entropy trainig.
# 2) frame cross-entropy training:
#    the objective is to classify frames to correct pdfs.
# 3) sequence-training optimizing sMBR: 
#    the objective is to emphasize state-sequences with better 
#    frame accuracy w.r.t. reference alignment.

# config
exp=
gmm=$exp/model/tri3b
output=

stage=0
affix=

nj=10
nj_decode="4"

train_set=train
test_set="test"

. ./utils/parse_options.sh || exit 1;
. ./cmd.sh ## You'll want to change cmd.sh to something that will work on your system.
           ## This relates to the queue.

. ./path.sh ## Source the tools/utils (import the queue.pl)

nj_decode_arr=($nj_decode)
test_set_arr=($test_set)

output=$output/nnet${affix}
mkdir -p $output

data_fmllr=$output/data-fmllr-tri3b

if [ $stage -le 0 ]; then
  # Store fMLLR features, so we can train on them easily,

  # test
  for i in "${!test_set_arr[@]}"; do
    dir=$data_fmllr/${test_set_arr[$i]}
    
    steps/nnet/make_fmllr_feats.sh --nj ${nj_decode_arr[$i]} --cmd "$train_cmd" \
      --transform-dir $gmm/decode_${test_set_arr[$i]} \
      $dir $exp/${test_set_arr[$i]} $gmm $dir/log $dir/data || exit 1
  done

  # train
  dir=$data_fmllr/${train_set}
  steps/nnet/make_fmllr_feats.sh --nj $nj --cmd "$train_cmd" \
     --transform-dir ${gmm}_ali \
     $dir $exp/${train_set} $gmm $dir/log $dir/data || exit 1

  # split the data : 90% train 10% cross-validation (held-out)
  utils/subset_data_dir_tr_cv.sh $dir ${dir}_tr90 ${dir}_cv10 || exit 1
fi

if [ $stage -le 1 ]; then
  # Pre-train DBN, i.e. a stack of RBMs
  dir=$output/dnn4b_pretrain-dbn
  (tail --pid=$$ -F $dir/log/pretrain_dbn.log 2>/dev/null)& # forward log
  $cuda_cmd $dir/log/pretrain_dbn.log steps/nnet/pretrain_dbn.sh --config conf/dnn.conf $data_fmllr/$train_set $dir || exit 1;
fi

if [ $stage -le 2 ]; then
  # Train the DNN optimizing per-frame cross-entropy.
  dir=$output/dnn4b_pretrain-dbn_dnn
  ali=${gmm}_ali
  feature_transform=$output/dnn4b_pretrain-dbn/final.feature_transform
  dbn=$output/dnn4b_pretrain-dbn/6.dbn
  
  (tail --pid=$$ -F $dir/log/train_nnet.log 2>/dev/null)& # forward log
  
  # Train
  $cuda_cmd $dir/log/train_nnet.log \
    steps/nnet/train.sh --feature-transform $feature_transform --dbn $dbn --hid-layers 0 --learn-rate 0.008 \
    $data_fmllr/${train_set}_tr90 $data_fmllr/${train_set}_cv10 $exp/lang $ali $ali $dir || exit 1;
    
  # Decode (reuse HCLG graph)
  for i in "${!test_set_arr[@]}"; do  
    steps/nnet/decode.sh --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" --config conf/decode.conf --acwt 0.1 \
      $gmm/graph $data_fmllr/${test_set_arr[$i]} $dir/decode_${test_set_arr[$i]} || exit 1;
  done
fi

# Sequence training using sMBR criterion, we do Stochastic-GD 
dir=$output/dnn4b_pretrain-dbn_dnn_smbr
srcdir=$output/dnn4b_pretrain-dbn_dnn
acwt=0.1

if [ $stage -le 3 ]; then
  # First we generate lattices and alignments:
  steps/nnet/align.sh --nj $nj --cmd "$train_cmd" \
    $data_fmllr/$train_set $exp/lang $srcdir ${srcdir}_ali || exit 1;
  steps/nnet/make_denlats.sh --nj $nj --cmd "$decode_cmd" --config conf/decode.conf --acwt $acwt \
    $data_fmllr/$train_set $exp/lang $srcdir ${srcdir}_denlats || exit 1;
fi

if [ $stage -le 4 ]; then
  # Re-train the DNN by 6 iterations of sMBR 
  steps/nnet/train_mpe.sh --cmd "$cuda_cmd" --num-iters 6 --acwt $acwt --do-smbr true \
    $data_fmllr/$train_set $exp/lang $srcdir ${srcdir}_ali ${srcdir}_denlats $dir || exit 1
  # Decode
  for ITER in 1 2 3 4 5 6; do
    for i in "${!test_set_arr[@]}"; do
      steps/nnet/decode.sh --nj ${nj_decode_arr[$i]} --cmd "$decode_cmd" --config conf/decode.conf \
        --nnet $dir/${ITER}.nnet --acwt $acwt \
        $gmm/graph $data_fmllr/${test_set_arr[$i]} $dir/decode_${test_set_arr[$i]}_it${ITER} || exit 1
    done
  done 
fi