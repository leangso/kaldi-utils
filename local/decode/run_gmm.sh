#!/bin/bash

stage=0

exp=work/result/exp_001
data=work/decode
output=work/decode-result
clean=false
test_sets=test
njs=1

# parse args
. ./utils/parse_options.sh

# initialize PATH
. ./path.sh  || die "path.sh $expected";

# initialize commands
. ./cmd.sh

test_sets=($test_sets)
njs=($njs)

# init output
[ "$clean" == true ] && rm -rf $output

# exp model
model=$exp/model
mono=$model/mono
tri=$model/tri1
lda=$model/tri2b
sat=$model/tri3b
sgmm=$model/sgmm

# decode model
model_decode=$output/model
mono_decode=$model_decode/mono
tri_decode=$model_decode/tri2a
lda_decode=$model_decode/tri2b      # lda-mllt
sat_decode=$model_decode/tri3b      # sat
sgmm_decode=$model_decode/sgmm

# make output directories
mkdir -p $output $model_decode

# init logging
exec > >(tee -i $output/log.txt)
exec 2>&1

#### Prepare Data ####
if [ $stage -le 1 ]; then
  printf ">>> 1. Preparing Dataset - $(date)\n"
  python3 local/prepare_data.py --data_dir $data --output_dir $output --test_sets ${test_sets[@]} || exit 1
fi

#### Compute MFCC ####
if [ $stage -le 2 ]; then
  printf "\n>>> 2. Computing MFCC - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n-->Processing $ds \n"   
    utils/fix_data_dir.sh $output/$ds || exit 1

    steps/make_mfcc.sh --njs ${njs[i]} --cmd "$train_cmd" $output/$ds || exit 1
    steps/compute_cmvn_stats.sh $output/$ds || exit 1

    utils/validate_data_dir.sh $output/$ds || exit 1
    i=$(expr $i + 1)
  done
fi

#### Monophone ####
if [ $stage -le 3 ]; then
  printf "\n>>> 3. Mono - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Decoding $ds \n"
    steps/decode.sh --config conf/decode.conf --njs ${njs[i]} --cmd "$decode_cmd" \
      $mono/graph $output/$ds $mono/test/decode_$ds || exit 1   
    i=$(expr $i + 1)
  done
  
  mv $mono/test/* $mono_decode 
fi

#### Triphone ####
if [ $stage -le 4 ]; then
  printf "\n>>> 4. Triphone - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Decoding $ds \n"
    steps/decode.sh --config conf/decode.conf --njs ${njs[i]} --cmd "$decode_cmd" \
      $tri/graph $output/$ds $tri/test/decode_$ds || exit 1  
    i=$(expr $i + 1)
  done
  
  mv $tri1/test/* $tri1_decode
fi

#### LDA-MLLT ####
if [ $stage -le 6 ]; then
  printf "\n>>> 6. LDA-MLLT - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Decoding $ds \n"
    steps/decode.sh --config conf/decode.conf --njs ${njs[i]} --cmd "$decode_cmd" \
      $lda/graph $output/$ds $lda/test/decode_$ds || exit 1
    i=$(expr $i + 1)
  done
  
  mv $lda/test/* $lda_decode
fi

#### SAT ####
if [ $stage -le 7 ]; then
  printf "\n>>> 7. SAT - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Decoding $ds \n"
    steps/decode_fmllr.sh --config conf/decode.conf --njs ${njs[i]} --cmd "$decode_cmd" \
      $sat/graph $output/$ds $sat/test/decode_$ds || exit 1
    i=$(expr $i + 1)
  done
  
  mv $sat/test/* $sat_decode
fi

#### SGMM ####
if [ $stage -le 8 ]; then
  printf "\n>>> 8. SGMM - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Decoding $ds \n"
    steps/decode_sgmm2.sh --config conf/decode.conf --njs ${njs[i]} --cmd "$decode_cmd" --transform-dir $sat_decode/decode_$ds \
      $sgmm/graph $output/$ds $sgmm/test/decode_$ds || exit 1
    i=$(expr $i + 1)
  done
  
  mv $sgmm/test/* $sgmm_decode
fi

# wait background process finish
wait

# score
for ds in ${test_sets[@]}; do
  printf "\n>>> WER - $ds \n"
  for x in $model_decode/*/decode_$ds; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
done