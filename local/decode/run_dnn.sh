#!/bin/bash

stage=3

exp=work/result/exp_001
data=work/decode
output=work/decode-result
test_sets=test
clean=false
njs=1
acwt=0.1
iter=6

# parse args
. ./utils/parse_options.sh

# initialize PATH
. ./path.sh  || die "path.sh $expected";

# initialize commands
. ./cmd.sh

test_sets=($test_sets)
njs=($njs)

# init output
[ "$clean" == true ] && rm -rf $output

# exp model
model=$exp/model
gmm=$model/tri3b
dnn=$model/dnn

# decode model
model_decode=$output/model
gmm_decode=$model_decode/tri3b
dnn_decode=$model_decode/dnn
dnn_data_fmllr=$dnn_decode/data-fmllr-tri3b

# make output directories
mkdir -p $output $model_decode

# init logging
exec > >(tee -i $output/dnn.log)
exec 2>&1

#### Prepare Data ####
if [ $stage -le 1 ]; then
  printf ">>> 1. Preparing Dataset - $(date)\n"
  python3 local/prepare_data.py --data_dir $data --output_dir $output --test_sets ${test_sets[@]} || exit 1
fi

#### Compute MFCC ####
if [ $stage -le 2 ]; then
  printf "\n>>> 2. Computing MFCC - $(date)"
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n-->Processing $ds \n"   
    utils/fix_data_dir.sh $output/$ds || exit 1

    steps/make_mfcc.sh --njs ${njs[i]} --cmd "$train_cmd" $output/$ds || exit 1
    steps/compute_cmvn_stats.sh $output/$ds || exit 1

    utils/validate_data_dir.sh $output/$ds || exit 1
    i=$(expr $i + 1)
  done
fi

if [ $stage -le 3 ]; then
  printf "\n>>> 3. Transform Feature - $(date)"
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Processing $ds \n"

    dir=$dnn_data_fmllr/$ds
    steps/nnet/make_fmllr_feats.sh --nj ${njs[i]} --cmd "$train_cmd" \
      --transform-dir $gmm_decode/decode_$ds $dir $ds $gmm $dir/log $dir/data || exit 1
      
    i=$(expr $i + 1)
  done
fi

if [ $stage -le 4 ]; then
  printf "\n>>> 4. DBN Decoding - $(date)"
  
  dir=$dnn_decode/dnn4b_pretrain-dbn_dnn
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Processing $ds \n"
  
    steps/nnet/decode.sh --nj ${njs[i]} --cmd "$decode_cmd" --config conf/decode_dnn.conf --acwt $acwt \
      $gmm/graph $dnn_data_fmllr/$ds $dir/decode_$ds || exit 1
      
    i=$(expr $i + 1)
  done
fi

if [ $stage -le 5 ]; then
  printf "\n>>> 5. sMBR Decoding - $(date)"
  
  dir=$dnn_decode/dnn4b_pretrain-dbn_dnn_smbr
  
  i=0
  for ds in ${test_sets[@]}; do
    printf "\n--> Processing $ds \n"
  
    steps/nnet/decode.sh --nj ${njs[i]} --cmd "$decode_cmd" --config conf/decode_dnn.conf \
      --nnet $dir/${iter}.nnet --acwt $acwt \
      $gmm/graph $dnn_data_fmllr/$ds $dir/decode_${ds}_it${iter} || exit 1
  
    i=$(expr $i + 1)
  done
fi

# wait background process finish
wait

# score
for ds in ${test_sets[@]}; do
  printf "\n>>> DBN WER - $ds \n"
  for x in $dnn_decode/dnn4b_pretrain-dbn_dnn/*/decode_$ds; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
done

printf "\n----------------------\n"

# score
for ds in ${test_sets[@]}; do
  printf "\n>>> sMBR WER - $ds \n"
  for x in $dnn_decode/dnn4b_pretrain-dbn_dnn_smbr/*/decode_$ds; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
done
