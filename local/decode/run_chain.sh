#!/bin/bash

stage=3

data=work/decode
output=work/result
version=001

model=work/result/exp_001/model/tdnn1k
tree_affix=

test_set="test"
clean=false
nj="4"

# parse args
. ./utils/parse_options.sh

nj_arr=($nj)
test_set_arr=($test_set)

decode=$output/decode_$version

# initialize PATH
. ./path.sh || die "path.sh $expected"

# initialize command
. ./cmd.sh

# init output
[ "$clean" == true ] && rm -rf $decode

# make output directories
mkdir -p $output $decode

# init logging
exec > >(tee -a $decode/log.txt)
exec 2>&1

#### Prepare Data ####
if [ $stage -le 1 ]; then
  printf ">>> 1. Preparing Data - $(date)"

  printf "\n--> Preparing Dataset\n"
  python3 local/prepare_data.py --data_dir $input --output_dir $decode --dataset $test_set || exit 1
fi

#### Compute MFCC ####
if [ $stage -le 2 ]; then
  printf "\n>>> 2. Computing MFCC - $(date)"
  for i in "${!test_set_arr[@]}"; do
    data=${test_set_arr[$i]}
    printf "\n-->Processing $data \n"

    utils/fix_data_dir.sh $decode/$data || exit 1

    steps/make_mfcc.sh --nj ${nj_arr[$i]} --cmd "$train_cmd" $decode/$data || exit 1
    steps/compute_cmvn_stats.sh $decode/$data || exit 1

    utils/validate_data_dir.sh $decode/$data || exit 1
  done
fi

if [ $stage -le 3 ]; then
  printf "\n>>> 3. Computing High-Resolution MFCC - $(date)"
  for i in "${!test_set_arr[@]}"; do
    data=${test_set_arr[$i]}
    printf "\n-->Processing $data \n"

    utils/copy_data_dir.sh $decode/$data $decode/${data}_hires

    utils/fix_data_dir.sh $decode/${data}_hires || exit 1

    steps/make_mfcc.sh --nj ${nj_arr[$i]} --cmd "$train_cmd" --mfcc-config conf/mfcc_hires.conf $decode/${data}_hires || exit 1
    steps/compute_cmvn_stats.sh $decode/${data}_hires || exit 1

    utils/validate_data_dir.sh $decode/${data}_hires || exit 1
  done
fi

if [ $stage -le 4 ]; then
  printf "\n>>> 4. Extract iVectors - $(date)"

  for i in "${!test_set_arr[@]}"; do
    data=${test_set_arr[$i]}

    printf "\n-->Processing $data \n"

    steps/online/nnet2/extract_ivectors_online.sh --cmd "$train_cmd" --nj ${nj_arr[$i]} \
      $decode/${data}_hires $model/extractor \
      $decode/ivectors_${data}_hires
  done
fi

if [ $stage -le 4 ]; then
  printf "\n>>> 4. Decoding - $(date)"

  lang=$model/lang_chain
  tree_dir=$model/tree_sp${tree_affix:+_$tree_affix}
  
  # note: if the features change (e.g. you add pitch features), you will have to
  # change the options of the following command line.
  steps/online/nnet3/prepare_online_decoding.sh \
    --mfcc-config conf/mfcc_hires.conf \
    $lang $model/extractor $model $decode

  rm $output/.error 2>/dev/null || true

  for i in "${!test_set_arr[@]}"; do
    (
      data=${test_set_arr[$i]}
      nspk=$(wc -l <$decode/${data}_hires/spk2utt)
      # note: we just give it "data/${data}" as it only uses the wav.scp, the
      # feature type does not matter.
      steps/online/nnet3/decode.sh \
        --acwt 1.0 --post-decode-acwt 10.0 \
        --nj ${nj_arr[$i]} --cmd "$decode_cmd" \
        $tree_dir/graph $decode/${data} $decode/decode_${data} || exit 1
        
#      steps/lmrescore_const_arpa.sh --cmd "$decode_cmd" \
#        data/lang_test_{tgsmall,tglarge} \
#       data/${data}_hires ${dir}_online/decode_{tgsmall,tglarge}_${data} || exit 1

    ) || touch $decode/.error &
  done
  wait
  [ -f $decode/.error ] && echo "$0: there was a problem while decoding" && exit 1
fi

# wait background process finish
wait

# score
for data in ${test_sets[@]}; do
  printf "\n>>> TDNN WER - $data \n"
  for x in $decode/*/decode*; do [ -d $x ] && grep WER $x/wer_* | utils/best_wer.sh; done
done
