import os
import random
import shutil

from collections import Counter
from os import path

import util as util


def load_spk_data(spk_dir):
    data = []

    spk_id = os.path.basename(spk_dir)

    trans_file = os.path.join(spk_dir, 'trans.csv')
    with open(trans_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            line = line.strip()

            parts = line.split(',')
            trans_id = parts[0]
            trans = parts[1].strip()

            data.append((trans_id, trans))
    return spk_id, data, spk_dir


def get_gender(spk_id):
    return spk_id[0]


def preprocess_trans(trans):
    return trans


def generate_dataset(dataset, dataset_name, output_dir):
    dataset_dir = path.join(output_dir, dataset_name)
    wav_dir = path.join(dataset_dir, 'wav')

    os.makedirs(dataset_dir, exist_ok=True)
    os.makedirs(wav_dir, exist_ok=True)

    trans_file = open(path.join(dataset_dir, 'trans.csv'), 'w', encoding='utf-8')

    male_spk_counter = 0
    female_spk_counter = 0

    print('Generate dataset: ', dataset_dir)
    for spk in dataset:
        spk_id = spk[0]
        data = spk[1]
        spk_dir = spk[2]

        gender = get_gender(spk_id)
        if gender == 'M':
            male_spk_counter += 1
        elif gender == 'F':
            female_spk_counter += 1
        else:
            print('spk unk: ', str(spk_id))

        print('Processing speaker: ', spk_id)

        to_spk_dir = path.join(wav_dir, spk_id)
        os.makedirs(to_spk_dir, exist_ok=True)

        for trans_id, trans in data:
            # trans
            utt_id = '%s-%04d' % (spk_id, int(trans_id))
            trans = preprocess_trans(trans)
            trans_file.write('%s, %s\n' % (utt_id, trans))

            # wav
            from_wav = path.join(spk_dir, '%s/wav/%04d.wav' % (spk_dir, int(trans_id)))
            to_wav = path.join(to_spk_dir, '%04d.wav' % int(trans_id))
            shutil.copyfile(from_wav, to_wav)

    print('Total speakers: ', (male_spk_counter + female_spk_counter))
    print('Male speakers: ', male_spk_counter)
    print('Female speakers: ', female_spk_counter)

    trans_file.close()


def generate_train_test(data_dirs, output_dir, nb_test_spk_male=None, nb_test_spk_female=None, random_seed=123, clean=False):
    random.seed(random_seed)

    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)

    if type(data_dirs) != list:
        data_dirs = [data_dirs]

    # load all datasets
    dataset = []
    for data_dir in data_dirs:
        for spk_dir in util.scan_dir(data_dir):
            if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
                continue
            
            print('Processing speaker...: ', spk_dir.name)
            dataset.append(load_spk_data(spk_dir.path))

    random.shuffle(dataset)

    # split train and test set
    train_set = []
    test_set = []

    test_spk_male_counter = 0
    test_spk_female_counter = 0

    for spk in dataset:
        gender = get_gender(spk[0])

        if nb_test_spk_male is not None and gender == 'M' and test_spk_male_counter < nb_test_spk_male:
            test_set.append(spk)
            test_spk_male_counter += 1
        elif nb_test_spk_female is not None and gender == 'F' and test_spk_female_counter < nb_test_spk_female:
            test_set.append(spk)
            test_spk_female_counter += 1
        else:
            train_set.append(spk)

    test_set = diff_trans_from_train(train_set, test_set)

    # generate datasets
    for dataset, dataset_name in zip([train_set, test_set], ['train', 'test']):
        generate_dataset(dataset, dataset_name, output_dir)


def generate_train_test_cspk(data_dirs, output_dir, train_spk='M', clean=False):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)

    if type(data_dirs) != list:
        data_dirs = [data_dirs]

    # load all datasets
    dataset = []
    for data_dir in data_dirs:
        for spk_dir in util.scan_dir(data_dir):
            if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
                continue

            print('Processing speaker: ', spk_dir.name)
            dataset.append(load_spk_data(spk_dir.path))

    train_set = []
    test_set = []

    for spk in dataset:
        gender = get_gender(spk[0])

        if gender == train_spk:
            train_set.append(spk)
        else:
            test_set.append(spk)
    
    # test_set = diff_trans_from_train(train_set, test_set)

    datasets = [train_set, test_set]
    dataset_names = ['train', 'test']

    for dataset, dataset_name in zip(datasets, dataset_names):
        generate_dataset(dataset, dataset_name, output_dir)


def generate_text_corpus(data_dirs, output_dir):
    os.makedirs(output_dir, exist_ok=True)

    if type(data_dirs) != list:
        data_dirs = [data_dirs]

    corpus, vocab = [], Counter()

    for data_dir in data_dirs:
        print('Processing data...: ', data_dir)
        
        # find speaker dirs
        for spk_dir in util.scan_dir(data_dir):
            if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
                continue
            
            print('Processing speaker...: ', spk_dir.name)

            trans_file = os.path.join(spk_dir.path, 'trans.csv')
            with open(trans_file, 'r', encoding='utf-8') as reader:
                for line in reader:
                    line = line.strip()
                    parts    = line.split(',')
                    trans    = parts[1].strip()

                    corpus.append(trans)
                    vocab.update(trans.split())

    # write corpus file
    corpus = list(set(corpus))
    corpus.sort()

    with open(path.join(output_dir, 'corpus.txt'), 'w', encoding='utf-8') as writer:
        for trans in corpus:
            writer.write('<s> %s </s>\n' % trans)

    # write vocab, count file
    vocab_w_count = sorted(vocab.most_common(), key=lambda item: item[1], reverse=True)
    with open(path.join(output_dir, 'vocab_n.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_w_count:
            writer.write('%s %s\n' % (word, n))

    # write vocab file
    vocab_wo_count = sorted(vocab.most_common(), key=lambda item: item[0])
    with open(path.join(output_dir, 'vocab.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_wo_count:
            writer.write('%s\n' % word)


def extract_datasets(data_dirs, output_dir, num_trans_per_spk=600, exclude_spks=[]):
    shutil.rmtree(output_dir, ignore_errors=True)

    os.makedirs(output_dir, exist_ok=True)

    if type(data_dirs) != list:
        data_dirs = [data_dirs]

    for data_dir in data_dirs:
        for spk_dir in util.scan_dir(data_dir):
            if (spk_dir.is_dir() is False) or spk_dir.name.startswith('.') or spk_dir.name.startswith('_') or (spk_dir.name in exclude_spks) :
                continue

            spk_id = os.path.basename(spk_dir)
            print('Processing speaker: ', spk_id)

            new_spk_id = '%s%s' % (spk_id[6], spk_id[11:])
            out_spk_dir = os.path.join(output_dir, new_spk_id)
            
            os.makedirs(out_spk_dir,  exist_ok=True)
            os.makedirs(os.path.join(out_spk_dir, 'wav'),  exist_ok=True)
            
            trans_file = os.path.join(spk_dir, 'trans_w.csv')
            
            data = []
            with open(trans_file, 'r', encoding='utf-8') as reader:
                for line in reader:
                    line = line.strip()

                    parts    = line.split(',')
                    trans_id = parts[0]
                    trans    = parts[1].strip()

                    data.append([trans_id, trans])
            
            random.shuffle(data)
        
            data = data[:num_trans_per_spk]
            data = sorted(data, key=lambda x: x[0])
            
            out_trans_file = os.path.join(out_spk_dir, 'trans.csv')
            out_trans_writer = open(out_trans_file, 'w', encoding='utf-8')
            
            for record in data:
                trans_id = record[0]
                trans = record[1]
                
                out_trans_writer.write('%s, %s\n' % (trans_id, trans))
                
                from_wav = os.path.join(spk_dir, 'wav', '%s.wav' % trans_id)
                to_wav = os.path.join(out_spk_dir, 'wav', '%s.wav' % trans_id)
                
                shutil.copyfile(from_wav, to_wav)
                
            out_trans_writer.close()


def get_speakers_from_file(path):
    spks = []
    with open(path, 'r', encoding='utf-8') as reader:
        for line in reader:
            line = line.strip()
            spks.append(line)
    return spks


def diff_trans_from_train(train_set, test_set):
    # train set
    train_transes = []

    for  spk_id, data, spk_dir  in train_set:
        for trans_id, trans in data:
            train_transes.append(trans)

    train_nb_trans = len(train_transes)

    train_transes = list(set(train_transes))

    # test set
    test_transes = []

    new_test_set = []
    new_transes = []

    for spk_id, data, spk_dir in test_set:
        new_data = []
        for ele in data:
            trans = ele[1]
            
            exist = False
            for train_trans in train_transes:
                if trans == train_trans:
                    exist = True
                    break
            
            if not exist:
                new_data.append(ele)
                new_transes.append(trans)

            test_transes.append(trans)
            
        new_test_set.append((spk_id, new_data, spk_dir))

    print('Train set: total=%s, unique=%s' % (train_nb_trans, len(train_transes)))
    print('Test set: total=%s, unique=%s' % (len(test_transes), len(list(set(test_transes)))))
    print('Diff set: total=%s, unique=%s' % (len(new_transes), len(list(set(new_transes)))))

    return new_test_set


if __name__ == '__main__':
    data_dir = '/home/leangso/work/'

    # exclude_spks = get_speakers_from_file('/home/leangso/work/data/Khmer/BTEC/speaker_exclude.txt')
    # extract_datasets(data_dirs='/home/leangso/work/data/Khmer/BTEC/NICT', output_dir='/home/leangso/work/data/Khmer/BTEC_EXP', num_trans_per_spk=600, exclude_spks=exclude_spks)    

    generate_train_test(data_dirs=[data_dir + '/data/Khmer/BTEC_EXP'],
                        output_dir=data_dir + '/exp/data/khmer/ds', nb_test_spk_male=5,
                        nb_test_spk_female=5, random_seed=111, clean=True)

    generate_train_test_cspk(data_dirs=[data_dir + '/data/Khmer/BTEC_EXP'],
                             output_dir=data_dir + '/exp/data/khmer/ds_female', train_spk='F', clean=True)

    generate_train_test_cspk(data_dirs=[data_dir + '/data/Khmer/BTEC_EXP'],
                             output_dir=data_dir + '/exp/data/khmer/ds_male', train_spk='M', clean=True)

    generate_text_corpus(data_dir + '/data/Khmer/BTEC_EXP', output_dir=data_dir + '/exp/data/khmer/lm')