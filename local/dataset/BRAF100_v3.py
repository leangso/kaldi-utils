import os
import random
import re
import shutil

from pydub import AudioSegment
from collections import Counter
from os import path

import util as util

random.seed(111)


def load_fo(fo_file):
    content = []
    with open(fo_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            line = line.strip()
            if line != '':
                content.append(line.split(':', 1))
    return content


def get_p_info(content):
    info, trans, is_trans = {}, '', False
    for ele in content:
        if ele[0] == 'SPI':
            data = ele[1].split(',')
            info['sex'] = data[0].strip()
            info['age'] = data[1].strip()
            info['lang'] = data[2].strip()
            info['code'] = data[3].strip()

            if info['sex'] not in ['H', 'F']:
                raise Exception('Invalid speaker gender:', info['sex'])

        elif ele[0] == 'LBR':
            data = ele[1].split(',')
            start, stop = data[0].strip(), data[1].strip()
            trans += ' '.join(data[5:]).strip() # TODO: should join with ','
        elif ele[0] == 'EXT':
            trans += ele[1]
    info['trans'] = [(start, stop, trans)]
    return info


def get_s_info(content):
    info, all_trans = {}, []
    for ele in content:
        if ele[0] == 'SPI':
            data = ele[1].split(',')
            info['sex'] = data[0].strip()
            info['age'] = data[1].strip()
            info['lang'] = data[2].strip()
            info['code'] = data[3].strip()

            if info['sex'] not in ['H', 'F']:
                raise Exception('Invalid speaker gender:', info['sex'])

        elif ele[0] == 'LBR':
            data = ele[1].split(',') # TODO: should join with ','
            trans = (data[0].strip(), data[1].strip(), ' '.join(data[5:]).strip())
            all_trans.append(trans)
    info['trans'] = all_trans
    return info


def load_raw_audio(audio_path, sample_width=2, frame_rate=16000, channels=1):
    return AudioSegment.from_raw(audio_path, sample_width=sample_width, frame_rate=frame_rate, channels=channels)


def get_sample_slice(audio, start_sample, end_sample):
    return audio.get_sample_slice(start_sample=start_sample, end_sample=end_sample)


def load_dataset(dataset_dir):
    dataset = []
    dataset_name = os.path.basename(dataset_dir)
    for spk_dir in util.scan_dir(dataset_dir):
        if not path.isdir(spk_dir.path):
            print('Invalid speaker directory: ', spk_dir.path)
            continue

        spk = []
        for file in util.scan_dir(spk_dir.path):
            if file.path.endswith(".PFO"):
                raw_audio = file.path.rsplit('.', 1)[0] + '.PFS'
                if not path.exists(raw_audio):
                    continue
                
                content = load_fo(file.path)
                session = get_p_info(content)
                session['audio'] = raw_audio
                session['spk_id'] = session['sex'] + session['code'] + '_' + dataset_name
                session['session_id'] = 'P' + os.path.basename(file.path).split('.')[0]

                spk.append(session)

            elif file.path.endswith(".SFO"):
                raw_audio = file.path.rsplit('.', 1)[0] + '.SFS'
                if not path.exists(raw_audio):
                    continue

                content = load_fo(file.path)
                session = get_s_info(content)
                session['audio'] = raw_audio
                session['spk_id'] = session['sex'] + session['code'] + '_' + dataset_name
                session['session_id'] = 'S' + os.path.basename(file.path).split('.')[0]

                spk.append(session)

        dataset.append(spk)
    return dataset


def preprocess_trans(sent):
    sent = sent.lower()

    sign_rgx = '^[T]+|\\s+[T]+|[T]+\\s+|[T]+$'.replace('T', '.,;:!?&\'\"()-')
    sent = re.sub(r'%s' % sign_rgx, ' ', sent)

    sent = re.sub(r'\s+', ' ', sent).strip()
    return sent


def generate_dataset(dataset, dataset_name, output_dir, generate_wav=True):
    os.makedirs(output_dir, exist_ok=True)

    wav_dir = path.join(output_dir, 'wav')
    os.makedirs(wav_dir, exist_ok=True)

    trans_file = open(path.join(output_dir, dataset_name + '.csv'), 'w', encoding='utf-8')
    
    male_spk_counter = 0
    female_spk_counter = 0

    print(f'Generate dataset: {dataset_name}')
    for spk in dataset:
        sex = spk[0]['sex']

        if sex == 'H':
            male_spk_counter += 1
        elif sex == 'F':
            female_spk_counter += 1
        else:
            print('spk unk; ', str(spk))

        spk_id = spk[0]['spk_id']
        if generate_wav:
            spk_dir = path.join(wav_dir, spk_id)
            os.makedirs(spk_dir, exist_ok=True)

        print('Processing speaker: ', spk_id)

        for session in spk:
            session_audio = load_raw_audio(session['audio'])
            session_id = session['session_id']

            for trans_id, trans_data in enumerate(session['trans']):
                # trans
                utt_id = '%s-%s-%04d' % (spk_id, session_id, trans_id)
                trans = preprocess_trans(trans_data[2])
                trans_file.write('%s\t%s\n' % (utt_id, trans))

                # wav
                if generate_wav:
                    audio = get_sample_slice(session_audio, int(trans_data[0]), int(trans_data[1]))
                    audio_file = path.join(spk_dir, '%s-%04d.wav' % (session_id, trans_id))
                    audio.export(audio_file, format='wav')

    print('Total speakers: ', (male_spk_counter + female_spk_counter))
    print('Male speakers: ', male_spk_counter)
    print('Female speakers: ', female_spk_counter)

    trans_file.close()


# def generate_train_test(dataset_dir, output_dir, nb_test_spk_male=None, nb_test_spk_female=None, random_seed=123, clean=False):
#     random.seed(random_seed)

#     # prepare output dir
#     if clean is True and path.exists(output_dir):
#         shutil.rmtree(output_dir)

#     if type(dataset_dir) != list:
#         dataset_dir = [dataset_dir]

#     datasets = []
#     for dir in dataset_dir:
#         print('Loading data: ', dir)
#         datasets.extend(load_dataset(dir))

#     random.shuffle(datasets)

#     train_set = []
#     test_set = []

#     test_spk_male_counter = 0
#     test_spk_female_counter = 0

#     for spk in datasets:
#         sex = spk[0]['sex']

#         if nb_test_spk_male is not None and sex == 'H' and test_spk_male_counter < nb_test_spk_male:
#             test_set.append(spk)
#             test_spk_male_counter += 1
#         elif nb_test_spk_female is not None and sex == 'F' and test_spk_female_counter < nb_test_spk_female:
#             test_set.append(spk)
#             test_spk_female_counter += 1
#         else:
#             train_set.append(spk)

#     generate_dataset(train_set, 'train', output_dir)
#     generate_dataset(test_set, 'test', output_dir)


# def generate_train_test_from_dir(train_dir, test_dir, output_dir, clean=False):
#     # prepare output dir
#     if clean is True and path.exists(output_dir):
#         shutil.rmtree(output_dir)
    
#     print('Loading train set: ', train_dir)
#     train_set = load_dataset(train_dir)

#     print('Loading test set: ', test_dir)
#     test_set = load_dataset(test_dir)

#     generate_dataset(train_set, 'train', output_dir)
#     generate_dataset(test_set, 'test', output_dir)


def generate_train_test(train_dir, test_dir, output_dir, clean=True):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    print('Loading train set: ', train_dir)
    train_set = load_dataset(train_dir)

    print('Loading test set: ', test_dir)
    test_set = load_dataset(test_dir)

    test_set_male = []
    test_set_female = []
    for spk in test_set:
        sex = spk[0]['sex']
        if sex == 'H':
            test_set_male.append(spk)
        else:
            test_set_female.append(spk)

    train_set_male = []
    train_set_female = []
    for spk in train_set:
        sex = spk[0]['sex']
        if sex == 'H':
            train_set_male.append(spk)
        else:
            train_set_female.append(spk)

    # train & test
    generate_dataset(train_set, 'train', output_dir)
    generate_dataset(test_set, 'test', output_dir)

    # train male and female
    generate_dataset(train_set_male, 'train_male', output_dir, generate_wav=False)
    generate_dataset(train_set_female, 'train_female', output_dir, generate_wav=False)

    # test male and female
    generate_dataset(test_set_male, 'test_male', output_dir, generate_wav=False)
    generate_dataset(test_set_female, 'test_female', output_dir, generate_wav=False)

    # male and female
    male_set = train_set_male + test_set_male
    female_set = train_set_female + test_set_female    
    generate_dataset(male_set, 'male', output_dir)
    generate_dataset(female_set, 'female', output_dir)


def generate_text_corpus(dataset_dir, output_dir):
    os.makedirs(output_dir, exist_ok=True)

    if type(dataset_dir) != list:
        dataset_dir = [dataset_dir]

    corpus, vocab = [], Counter()

    for dir in dataset_dir:
        print('Loading data: ', dir)
        dataset = load_dataset(dir)

        print('Processing data: ', dir)
        for spk in dataset:
            for session in spk:
                for trans_data in session['trans']:
                    trans = preprocess_trans(trans_data[2])
                    corpus.append(trans)
                    vocab.update(trans.split())

    # write corpus file
    corpus = list(set(corpus))
    corpus.sort()

    with open(path.join(output_dir, 'corpus.txt'), 'w', encoding='utf-8') as writer:
        for trans in corpus:
            writer.write('<s> %s </s>\n' % trans)

    # write vocab, count file
    vocab_w_count = sorted(vocab.most_common(), key=lambda item: item[1], reverse=True)
    with open(path.join(output_dir, 'vocab_n.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_w_count:
            writer.write('%s %s\n' % (word, n))

    # write vocab file
    vocab_wo_count = sorted(vocab.most_common(), key=lambda item: item[0])
    with open(path.join(output_dir, 'vocab.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_wo_count:
            writer.write('%s\n' % word)


def generate_train_test_equal_size(train_dir, test_dir, output_dir, clean=True):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir)

    print('Loading train set: ', train_dir)
    train = load_dataset(train_dir)

    random.shuffle(train)

    # first male and female
    first_male = None
    first_female = None
    for spk in train:
        sex = spk[0]['sex']
        if sex == 'H':
            first_male = spk[0]['code']
        else:
            first_female = spk[0]['code']

        if first_male != None and first_female != None:
            break

    spk_exclude_file = os.path.join(output_dir, 'spk_excl.txt')
    with open(spk_exclude_file, 'w', encoding='utf-8') as writer:
        writer.write('H%s\n' % first_male)
        writer.write('F%s\n' % first_female)

    train = [spk for spk in train if spk[0]['code'] != first_male and spk[0]['code'] != first_female]

    print('Loading test set: ', test_dir)
    test = load_dataset(test_dir)

    # normal
    random.shuffle(train)
    
    train_mix1 = []
    train_mix2 = []

    male_spk_counter = 0
    female_spk_counter = 0
    for spk in train:
        sex = spk[0]['sex']
        if sex == 'H':
            if male_spk_counter < 22:
                train_mix1.append(spk)
                male_spk_counter += 1
            else:
                train_mix2.append(spk)
        else:
            if female_spk_counter < 22:
                train_mix1.append(spk)
                female_spk_counter += 1
            else:
                train_mix2.append(spk)

    # cross
    train_male = []
    train_female = []
    for spk in train:
        sex = spk[0]['sex']
        if sex == 'H':
            train_male.append(spk)
        else:
            train_female.append(spk)
    
    # train mix
    generate_dataset(train_mix1, 'train_mix1', output_dir)
    generate_dataset(train_mix2, 'train_mix2', output_dir)

    # train male, train female
    generate_dataset(train_male, 'train_male', output_dir, generate_wav=False)
    generate_dataset(train_female, 'train_female', output_dir, generate_wav=False)
    
    # test male and female
    test_male = []
    test_female = []
    for spk in test:
        sex = spk[0]['sex']
        if sex == 'H':
            test_male.append(spk)
        else:
            test_female.append(spk)

    generate_dataset(test_male, 'test_male', output_dir)
    generate_dataset(test_female, 'test_female', output_dir)


def correct_encoding(dataset_dir, output_dir, clean=False):
    if type(dataset_dir) != list:
        dataset_dir = [dataset_dir]

    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)

    for dir in dataset_dir:
        print('Preprocess dataset: ', dir)
        for spk_dir in util.scan_dir(dir):
            if not path.isdir(spk_dir.path):
                print('Invalid speaker directory: ', spk_dir.path)
                continue

            print('Preprocess speaker: ', spk_dir.name)

            output_spk_dir = path.join(output_dir, spk_dir.name)
            os.makedirs(output_spk_dir, exist_ok=True)

            for file in util.scan_dir(spk_dir.path):
                if file.path.endswith(".PFO") or file.path.endswith(".SFO"):
                    trans_file = open(file.path, 'r', encoding='latin-1')
                    output_trans_file = open(path.join(output_spk_dir, file.name), 'w', encoding='utf-8')

                    lines = trans_file.readlines()
                    output_trans_file.writelines(lines)

                    trans_file.close()
                    output_trans_file.close()

                else:
                    file_des_path = path.join(output_spk_dir, file.name)
                    shutil.copy(file.path, file_des_path)


if __name__ == '__main__':
    data_dir = '/home/leangso/work/'

    # generate_train_test(train_dir=data_dir + '/data/French/BRAF100/Train', test_dir=data_dir + '/data/French/BRAF100/Test',
    #                          output_dir=data_dir + '/exp/data/french/ds')

    generate_train_test_equal_size(train_dir=data_dir + '/data/French/BRAF100/Train', test_dir=data_dir + '/data/French/BRAF100/Test',
                             output_dir=data_dir + '/exp/data_v3/french/ds')

    generate_text_corpus(dataset_dir=[data_dir + '/data/French/BRAF100/Train', data_dir + '/data/French/BRAF100/Test'], output_dir=data_dir + '/exp/data_v3/french/lm')

    # generate_text_corpus(dataset_dir=[data_dir + '/data/French/BRAF100/Train'], output_dir=data_dir + '/exp/data/french/lm_train')

