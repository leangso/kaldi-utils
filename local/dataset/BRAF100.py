import os
import random
import re
import shutil

from pydub import AudioSegment
from collections import Counter
from os import path

import util as util


def load_fo(fo_file):
    content = []
    with open(fo_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            line = line.strip()
            if line != '':
                content.append(line.split(':', 1))
    return content


def get_p_info(content):
    info, trans, is_trans = {}, '', False
    for ele in content:
        if ele[0] == 'LBR':
            data = ele[1].split(',')
            start, stop = data[0].strip(), data[1].strip()
            trans += ' '.join(data[5:]).strip()
            is_trans = True
        elif ele[0] == 'SPI':
            data = ele[1].split(',')
            info['sex'] = data[0].strip()
            info['age'] = data[1].strip()
            info['lang'] = data[2].strip()
            info['code'] = data[3].strip()
        elif is_trans:
            trans += ele[1]
        elif ele[0] == 'LBR':
            break
    info['trans'] = [(start, stop, trans)]
    return info


def get_s_info(content):
    info, all_trans = {}, []
    for ele in content:
        if ele[0] == 'LBR':
            data = ele[1].split(',')
            trans = (data[0].strip(), data[1].strip(), ' '.join(data[5:]).strip())
            all_trans.append(trans)
        elif ele[0] == 'SPI':
            data = ele[1].split(',')
            info['sex'] = data[0].strip()
            info['age'] = data[1].strip()
            info['lang'] = data[2].strip()
            info['code'] = data[3].strip()
    info['trans'] = all_trans
    return info


def load_raw_audio(audio_path, sample_width=2, frame_rate=16000, channels=1):
    return AudioSegment.from_raw(audio_path, sample_width=sample_width, frame_rate=frame_rate, channels=channels)


def get_sample_slice(audio, start_sample, end_sample):
    return audio.get_sample_slice(start_sample=start_sample, end_sample=end_sample)


def load_dataset(dataset_dir):
    dataset = []
    for spk_dir in util.scan_dir(dataset_dir):
        if not path.isdir(spk_dir.path):
            print('Invalid speaker directory: ', spk_dir.path)
            continue

        sessions = []
        for file in util.scan_dir(spk_dir.path):
            if file.path.endswith(".PFO"):
                raw_audio = file.path.rsplit('.', 1)[0] + '.PFS'
                if not path.exists(raw_audio):
                    continue
                
                content = load_fo(file.path)
                info = get_p_info(content)
                info['audio'] = raw_audio
                info['ref'] = os.path.basename(file.path)

                sessions.append(info)

            elif file.path.endswith(".SFO"):
                raw_audio = file.path.rsplit('.', 1)[0] + '.SFS'
                if not path.exists(raw_audio):
                    continue

                content = load_fo(file.path)
                info = get_s_info(content)
                info['audio'] = raw_audio
                info['ref'] = os.path.basename(file.path)

                sessions.append(info)

        dataset.append(sessions)
    return dataset


def preprocess_trans(sent):
    sent = sent.lower()

    # sign_rgx = '^[T]+|\\s+[T]+|[T]+\\s+|[T]+$'.replace('T', '.,;:!?&\'\"()-')
    # sign_rgx = '^[T]+|\\s+[T]+|[T]+\\s+|[T]+$'.replace('T', '.,;:!?&\'\"()-')
    # sent = re.sub(r'%s' % sign_rgx, ' ', sent)

    sent = re.sub(r'\s+', ' ', sent).strip()
    return sent


def generate_dataset(dataset, dataset_name, output_dir):
    dataset_dir = path.join(output_dir, dataset_name)
    os.makedirs(dataset_dir, exist_ok=True)

    wav_dir = path.join(dataset_dir, 'wav')
    os.makedirs(wav_dir, exist_ok=True)

    trans_file = open(path.join(dataset_dir, 'trans.csv'), 'w', encoding='utf-8')
    ref_file = open(path.join(dataset_dir, 'ref.txt'), 'w', encoding='utf-8')

    male_spk_counter = 0
    female_spk_counter = 0

    print(f'Generate {dataset_name} set: {dataset_dir}')
    for spk_sessions in dataset:
        sex = spk_sessions[0]['sex']

        if sex == 'H':
            male_spk_counter += 1
        elif sex == 'F':
            female_spk_counter += 1
        else:
            print('spk unk; ', str(spk_sessions))

        spk_id = '%s%s%s' % (sex, spk_sessions[0]['code'], spk_sessions[0]['age'])
        spk_dir = path.join(wav_dir, spk_id)

        print('Processing speaker: ', spk_id)

        os.makedirs(spk_dir, exist_ok=True)

        for session_idx, session in enumerate(spk_sessions):
            session_audio = load_raw_audio(session['audio'])
            ref = session['ref']

            for trans_idx, trans_data in enumerate(session['trans']):
                # trans
                utt_id = '%s-%02d%03d' % (spk_id, session_idx, trans_idx)
                trans = preprocess_trans(trans_data[2])
                trans_file.write('%s, %s\n' % (utt_id, trans))

                # ref
                ref_file.write('%s, %s#%s\n' % (utt_id, ref, trans_idx))

                # wav
                audio = get_sample_slice(session_audio, int(trans_data[0]), int(trans_data[1]))
                audio_file = path.join(spk_dir, '%02d%03d.wav' % (session_idx, trans_idx))
                audio.export(audio_file, format='wav')

    print('Total speakers: ', (male_spk_counter + female_spk_counter))
    print('Male speakers: ', male_spk_counter)
    print('Female speakers: ', female_spk_counter)

    trans_file.close()
    ref_file.close()


def generate_train_test(data_dir, output_dir, nb_test_spk_male=None, nb_test_spk_female=None, random_seed=123, clean=False):
    random.seed(random_seed)

    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    if type(data_dir) != list:
        data_dir = [data_dir]

    dataset = []
    for data in data_dir:
        print('Loading data: ', data)
        dataset += load_dataset(data)

    random.shuffle(dataset)

    train_set = []
    test_set = []

    test_spk_male_counter = 0
    test_spk_female_counter = 0

    for spk_sessions in dataset:
        sex = spk_sessions[0]['sex']

        if nb_test_spk_male is not None and sex == 'H' and test_spk_male_counter < nb_test_spk_male:
            test_set.append(spk_sessions)
            test_spk_male_counter += 1
        elif nb_test_spk_female is not None and sex == 'F' and test_spk_female_counter < nb_test_spk_female:
            test_set.append(spk_sessions)
            test_spk_female_counter += 1
        else:
            train_set.append(spk_sessions)

    for ds, ds_name in zip([train_set, test_set], ['train', 'test']):
        generate_dataset(ds, ds_name, output_dir)


def generate_train_test_from_dir(train_dir, test_dir, output_dir, clean=False):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)
    
    print('Loading train set: ', train_dir)
    train = load_dataset(train_dir)

    print('Loading test set: ', test_dir)
    test = load_dataset(test_dir)

    for ds, ds_name in zip([train, test], ['train', 'test']):
        generate_dataset(ds, ds_name, output_dir)


def generate_train_test_cspk(data_dir, output_dir, train_spk='H', clean=False):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    if type(data_dir) != list:
        data_dir = [data_dir]

    dataset = []
    for data in data_dir:
        print('Loading data: ', data)
        dataset += load_dataset(data)

    train_set = []
    test_set = []
    
    for spk_sessions in dataset:
        sex = spk_sessions[0]['sex']

        if sex == train_spk:
            train_set.append(spk_sessions)
        else:
            test_set.append(spk_sessions)

    dss = [train_set, test_set]
    ds_names = ['train', 'test']

    for ds, ds_name in zip(dss, ds_names):
        generate_dataset(ds, ds_name, output_dir)


def generate_text_corpus(data_dir, output_dir):
    os.makedirs(output_dir, exist_ok=True)

    if type(data_dir) != list:
        data_dir = [data_dir]

    corpus, vocab = [], Counter()

    for data in data_dir:
        print('Loading data: ', data)
        dataset = load_dataset(data)

        print('Processing data: ', data)
        for spk_sessions in dataset:
            for session in spk_sessions:
                for trans_data in session['trans']:
                    trans = preprocess_trans(trans_data[2])
                    corpus.append(trans)
                    vocab.update(trans.split())

    # write corpus file
    corpus = list(set(corpus))
    corpus.sort()

    with open(path.join(output_dir, 'corpus.txt'), 'w', encoding='utf-8') as writer:
        for trans in corpus:
            writer.write('<s> %s </s>\n' % trans)

    # write vocab, count file
    vocab_w_count = sorted(vocab.most_common(), key=lambda item: item[1], reverse=True)
    with open(path.join(output_dir, 'vocab_n.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_w_count:
            writer.write('%s %s\n' % (word, n))

    # write vocab file
    vocab_wo_count = sorted(vocab.most_common(), key=lambda item: item[0])
    with open(path.join(output_dir, 'vocab.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_wo_count:
            writer.write('%s\n' % word)


def generate_text_corpus_from_trans_file(trans_file, output_dir):
    os.makedirs(output_dir, exist_ok=True)

    if type(trans_file) != list:
        trans_file = [trans_file]

    corpus, vocab = [], Counter()

    for file in trans_file:
        print('Loading data...', file)
        
        with open(file, 'r', encoding='utf-8') as reader:
            for line in reader:
                line = line.strip()
                ele = line.split(',', 1)
                trans = ele[1]

                corpus.append(trans)
                vocab.update(trans.split())

    # write corpus file
    corpus = list(set(corpus))
    corpus.sort()

    with open(path.join(output_dir, 'corpus.txt'), 'w', encoding='utf-8') as writer:
        for trans in corpus:
            writer.write('<s> %s </s>\n' % trans)

    # write vocab, count file
    vocab_w_count = sorted(vocab.most_common(), key=lambda item: item[1], reverse=True)
    with open(path.join(output_dir, 'vocab_n.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_w_count:
            writer.write('%s %s\n' % (word, n))

    # write vocab file
    vocab_wo_count = sorted(vocab.most_common(), key=lambda item: item[0])
    with open(path.join(output_dir, 'vocab.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_wo_count:
            writer.write('%s\n' % word)


def correct_encoding(data_dir, output_dir, clean=False):
    if type(data_dir) != list:
        data_dir = [data_dir]

    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)

    for data in data_dir:
        print('Preprocess dataset: ', data)
        for spk_dir in util.scan_dir(data):
            if not path.isdir(spk_dir.path):
                print('Invalid speaker directory: ', spk_dir.path)
                continue

            print('Preprocess speaker: ', spk_dir.name)

            output_spk_dir = path.join(output_dir, spk_dir.name)
            os.makedirs(output_spk_dir, exist_ok=True)

            for file in util.scan_dir(spk_dir.path):
                if file.path.endswith(".PFO") or file.path.endswith(".SFO"):
                    trans_file = open(file.path, 'r', encoding='latin-1')
                    output_trans_file = open(path.join(output_spk_dir, file.name), 'w', encoding='utf-8')

                    lines = trans_file.readlines()
                    output_trans_file.writelines(lines)

                    trans_file.close()
                    output_trans_file.close()

                else:
                    file_des_path = path.join(output_spk_dir, file.name)
                    shutil.copy(file.path, file_des_path)


if __name__ == '__main__':
    data_dir = '/home/leangso/work/'

    # generate_train_test(data_dir=[data_dir + '/data/BRAF100/Train', data_dir + '/data/BRAF100/Test'],
    #                     output_dir=data_dir + '/exp/data/french/ds', nb_test_spk_male=5,
    #                     nb_test_spk_female=5, random_seed=111, clean=True)
 
    generate_train_test_from_dir(train_dir=data_dir + '/data/French/BRAF100/Train', test_dir=data_dir + '/data/French/BRAF100/Test',
                        output_dir=data_dir + '/exp/data/french/ds', clean=True)
    
    generate_train_test_cspk(data_dir=[data_dir + '/data/French/BRAF100/Train', data_dir + '/data/French/BRAF100/Test'],
                             output_dir=data_dir + '/exp/data/french/ds_male', train_spk='H', clean=True)

    generate_train_test_cspk(data_dir=[data_dir + '/data/French/BRAF100/Train', data_dir + '/data/French/BRAF100/Test'],
                             output_dir=data_dir + '/exp/data/french/ds_female', train_spk='F', clean=True)

    generate_text_corpus(data_dir=[data_dir + '/data/French/BRAF100/Train', data_dir + '/data/French/BRAF100/Test'], output_dir=data_dir + '/exp/data/french/lm')

    # trans_file_2_text_corpus(trans_file=['/home/leangso/work/kaldi-utils/work/data/ds/train/trans.csv'], output_dir='/home/leangso/work/kaldi-utils/work/pre/lm')
