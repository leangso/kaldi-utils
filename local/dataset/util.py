import os

from os import path
from collections import Counter
from operator import itemgetter


def scan_dir(path):
    dirs = list(os.scandir(path))
    dirs.sort(key=lambda x: x.name)
    return dirs


def generate_text_corpus_from_trans_file(trans_file, output_dir):
    os.makedirs(output_dir, exist_ok=True)

    if type(trans_file) != list:
        trans_file = [trans_file]

    corpus, vocab = [], Counter()

    for file in trans_file:
        print('Loading data...', file)
        
        with open(file, 'r', encoding='utf-8') as reader:
            for line in reader:
                line = line.strip()
                ele = line.split('\t', 1)
                trans = ele[1]

                corpus.append(trans)
                vocab.update(trans.split())

    # write corpus file
    corpus = list(set(corpus))
    corpus.sort()

    with open(path.join(output_dir, 'corpus.txt'), 'w', encoding='utf-8') as writer:
        for trans in corpus:
            writer.write('<s> %s </s>\n' % trans)

    # write vocab, count file
    vocab_w_count = sorted(vocab.most_common(), key=lambda item: item[1], reverse=True)
    with open(path.join(output_dir, 'vocab_n.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_w_count:
            writer.write('%s %s\n' % (word, n))

    # write vocab file
    vocab_wo_count = sorted(vocab.most_common(), key=lambda item: item[0])
    with open(path.join(output_dir, 'vocab.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_wo_count:
            writer.write('%s\n' % word)


def generate_vocab(corpus_file, vocab_file):
    vocab_counter = Counter()
    print('Read corpus %s' % corpus_file)
    with open(corpus_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            words = line.strip().split()
            vocab_counter.update(words)

    # write vocab
    print('Write vocab %s ' % vocab_file)
    with open(vocab_file, 'w', encoding='utf-8') as writer:
        lexicon = sorted(vocab_counter.items(), key=itemgetter(0))
        for word, n in lexicon:
            if word in ['<s>', '</s>']:
                continue
            writer.write('%s\n' % word)