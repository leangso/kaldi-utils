import os
import random
import shutil

from collections import Counter
from os import path

import util as util

random.seed(111)


def load_spk_data(spk_dir):
    data = []

    spk_id = os.path.basename(spk_dir)
    spk_id = spk_id.replace('-', '_')
    gender = spk_id[6:7]

    trans_file = os.path.join(spk_dir, 'trans.csv')
    with open(trans_file, 'r', encoding='utf-8') as reader:
        for line in reader:
            line = line.strip()

            parts = line.split('\t')
            trans_id = parts[0]
            trans = parts[1].strip()

            data.append((trans_id, trans))
    return spk_id, gender, data, spk_dir


def preprocess_trans(trans):
    return trans


def generate_dataset(dataset, dataset_name, output_dir, generate_wav=True):
    os.makedirs(output_dir, exist_ok=True)

    wav_dir = path.join(output_dir, 'wav')
    os.makedirs(wav_dir, exist_ok=True)

    trans_file = open(path.join(output_dir, dataset_name + '.csv'), 'w', encoding='utf-8')

    male_spk_counter = 0
    female_spk_counter = 0

    print(f'Generate dataset: ', dataset_name)
    for spk in dataset:
        spk_id = spk[0]
        gender = spk[1]
        data = spk[2]
        spk_dir = spk[3]

        if gender == 'M':
            male_spk_counter += 1
        elif gender == 'F':
            female_spk_counter += 1
        else:
            print('spk unk: ', str(spk_id))

        if generate_wav:
            to_spk_dir = path.join(wav_dir, spk_id)
            os.makedirs(to_spk_dir, exist_ok=True)

        print('Processing speaker: ', spk_id)

        for trans_id, trans in data:
            # trans
            utt_id = '%s-%04d' % (spk_id, int(trans_id))
            trans = preprocess_trans(trans)
            trans_file.write('%s\t%s\n' % (utt_id, trans))

            # wav
            if generate_wav:
                from_wav = path.join(spk_dir, '%s/wav/%04d.wav' % (spk_dir, int(trans_id)))
                to_wav = path.join(to_spk_dir, '%04d.wav' % int(trans_id))
                shutil.copyfile(from_wav, to_wav)

    print('Total speakers: ', (male_spk_counter + female_spk_counter))
    print('Male speakers: ', male_spk_counter)
    print('Female speakers: ', female_spk_counter)

    trans_file.close()


def generate_train_test(data_dir, output_dir, nb_test_spk=5, clean=False):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)

    # load all datasets
    dataset = []
    for spk_dir in util.scan_dir(data_dir):
        if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
            continue
        
        print('Processing speaker...: ', spk_dir.name)
        dataset.append(load_spk_data(spk_dir.path))

    random.shuffle(dataset)

    # prepare train and test set
    train_set = []
    test_set = []
    test_spk_male_counter = 0
    test_spk_female_counter = 0
    for spk in dataset:
        gender = spk[1]

        if gender == 'M' and test_spk_male_counter < nb_test_spk:
            test_set.append(spk)
            test_spk_male_counter += 1
        elif gender == 'F' and test_spk_female_counter < nb_test_spk:
            test_set.append(spk)
            test_spk_female_counter += 1
        else:
            train_set.append(spk)

    # split gender for test set
    test_set_male = []
    test_set_female = []
    for spk in test_set:
        gender = spk[1]

        if gender == 'M':
            test_set_male.append(spk)
        elif gender == 'F':
            test_set_female.append(spk)
        else:
            raise Exception('Invalid speaker gender:', gender)

    # split gender for train set
    train_set_male = []
    train_set_female = []
    for spk in train_set:
        gender = spk[1]

        if gender == 'M':
            train_set_male.append(spk)
        elif gender == 'F':
            train_set_female.append(spk)
        else:
            raise Exception('Invalid speaker gender:', gender)

    # train & test
    generate_dataset(train_set, 'train', output_dir)
    generate_dataset(test_set, 'test', output_dir)

    # train male and female
    generate_dataset(train_set_male, 'train_male', output_dir, generate_wav=False)
    generate_dataset(train_set_female, 'train_female', output_dir, generate_wav=False)

    # test male and female
    generate_dataset(test_set_male, 'test_male', output_dir, generate_wav=False)
    generate_dataset(test_set_female, 'test_female', output_dir, generate_wav=False)

    # male and female
    male_set = train_set_male + test_set_male
    female_set = train_set_female + test_set_female    
    generate_dataset(male_set, 'male', output_dir)
    generate_dataset(female_set, 'female', output_dir)


def generate_train_test2(data_dir, output_dir, clean=False):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)

    # load all datasets
    dataset = []
    for spk_dir in util.scan_dir(data_dir):
        if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
            continue
        
        print('Processing speaker...: ', spk_dir.name)
        dataset.append(load_spk_data(spk_dir.path))

    random.shuffle(dataset)

    test_spks = [
        'KM_01_F_30_10002',
        'KM_01_F_30_10012',
        'KM_01_F_30_10014',
        'KM_01_F_30_10025',
        'KM_01_F_30_10026',
        'KM_01_M_30_10004',
        'KM_01_M_30_10009',
        'KM_01_M_30_10018',
        'KM_01_M_30_10020',
        'KM_01_M_30_10023'] 

    # prepare train and test set
    train_set = []
    test_set = []
    for spk in dataset:
        spk_id = spk[0]
        if spk_id in test_spks:
            test_set.append(spk)
        else:
            train_set.append(spk)

    # split gender for test set
    test_set_male = []
    test_set_female = []
    for spk in test_set:
        gender = spk[1]

        if gender == 'M':
            test_set_male.append(spk)
        elif gender == 'F':
            test_set_female.append(spk)
        else:
            raise Exception('Invalid speaker gender:', gender)

    # split gender for train set
    train_set_male = []
    train_set_female = []
    for spk in train_set:
        gender = spk[1]

        if gender == 'M':
            train_set_male.append(spk)
        elif gender == 'F':
            train_set_female.append(spk)
        else:
            raise Exception('Invalid speaker gender:', gender)

    # train & test
    generate_dataset(train_set, 'train', output_dir)
    generate_dataset(test_set, 'test', output_dir)

    # train male and female
    generate_dataset(train_set_male, 'train_male', output_dir, generate_wav=False)
    generate_dataset(train_set_female, 'train_female', output_dir, generate_wav=False)

    # test male and female
    generate_dataset(test_set_male, 'test_male', output_dir, generate_wav=False)
    generate_dataset(test_set_female, 'test_female', output_dir, generate_wav=False)

    # male and female
    male_set = train_set_male + test_set_male
    female_set = train_set_female + test_set_female    
    generate_dataset(male_set, 'male', output_dir)
    generate_dataset(female_set, 'female', output_dir)


# def generate_train_test_cspk(data_dirs, output_dir, train_spk='M', clean=False):
#     # prepare output dir
#     if clean is True and path.exists(output_dir):
#         shutil.rmtree(output_dir)

#     os.makedirs(output_dir, exist_ok=True)

#     if type(data_dirs) != list:
#         data_dirs = [data_dirs]

#     # load all datasets
#     dataset = []
#     for data_dir in data_dirs:
#         for spk_dir in util.scan_dir(data_dir):
#             if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
#                 continue

#             print('Processing speaker: ', spk_dir.name)
#             dataset.append(load_spk_data(spk_dir.path))

#     train_set = []
#     test_set = []

#     for spk in dataset:
#         gender = get_gender(spk[0])

#         if gender == train_spk:
#             train_set.append(spk)
#         else:
#             test_set.append(spk)
    
#     # test_set = diff_trans_from_train(train_set, test_set)

#     datasets = [train_set, test_set]
#     dataset_names = ['train', 'test']

#     for dataset, dataset_name in zip(datasets, dataset_names):
#         generate_dataset(dataset, dataset_name, output_dir)


def generate_text_corpus(data_dirs, output_dir):
    os.makedirs(output_dir, exist_ok=True)

    if type(data_dirs) != list:
        data_dirs = [data_dirs]

    corpus, vocab = [], Counter()

    for data_dir in data_dirs:
        print('Processing data...: ', data_dir)
        
        # find speaker dirs
        for spk_dir in util.scan_dir(data_dir):
            if not spk_dir.is_dir() or spk_dir.name.startswith('.') or spk_dir.name.startswith('_'):
                continue
            
            print('Processing speaker...: ', spk_dir.name)

            trans_file = os.path.join(spk_dir.path, 'trans.csv')
            with open(trans_file, 'r', encoding='utf-8') as reader:
                for line in reader:
                    line = line.strip()
                    parts = line.split('\t')
                    trans = parts[1].strip()

                    corpus.append(trans)
                    vocab.update(trans.split())

    # write corpus file
    corpus = list(set(corpus))
    corpus.sort()

    with open(path.join(output_dir, 'corpus.txt'), 'w', encoding='utf-8') as writer:
        for trans in corpus:
            writer.write('<s> %s </s>\n' % trans)

    # write vocab, count file
    vocab_w_count = sorted(vocab.most_common(), key=lambda item: item[1], reverse=True)
    with open(path.join(output_dir, 'vocab_n.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_w_count:
            writer.write('%s %s\n' % (word, n))

    # write vocab file
    vocab_wo_count = sorted(vocab.most_common(), key=lambda item: item[0])
    with open(path.join(output_dir, 'vocab.txt'), 'w', encoding='utf-8') as writer:
        for word, n in vocab_wo_count:
            writer.write('%s\n' % word)


def extract_dataset(data_dir, output_dir, num_trans_per_spk=700, exclude_spks=[], clean=False):
    # prepare output dir
    if clean is True and path.exists(output_dir):
        shutil.rmtree(output_dir)

    os.makedirs(output_dir, exist_ok=True)
    
    for spk_dir in util.scan_dir(data_dir):
        if (spk_dir.is_dir() is False) or spk_dir.name.startswith('.') or spk_dir.name.startswith('_') or (spk_dir.name in exclude_spks) :
            continue

        spk_id = os.path.basename(spk_dir)
        print('Processing speaker: ', spk_id)

        new_spk_id = spk_dir.name.replace('-', '_')
        out_spk_dir = os.path.join(output_dir, new_spk_id)
        
        os.makedirs(out_spk_dir,  exist_ok=True)
        os.makedirs(os.path.join(out_spk_dir, 'wav'),  exist_ok=True)
        
        trans_file = os.path.join(spk_dir, 'trans_w.csv')
        
        data = []
        with open(trans_file, 'r', encoding='utf-8') as reader:
            for line in reader:
                line = line.strip()

                parts    = line.split(',')
                trans_id = parts[0]
                trans    = parts[1].strip()

                data.append([trans_id, trans])
        
        random.shuffle(data)
    
        data = data[:num_trans_per_spk]
        data = sorted(data, key=lambda x: x[0])
        
        out_trans_file = os.path.join(out_spk_dir, 'trans.csv')
        out_trans_writer = open(out_trans_file, 'w', encoding='utf-8')
        
        for record in data:
            trans_id = record[0]
            trans = record[1]
            
            out_trans_writer.write('%s\t%s\n' % (trans_id, trans))
            
            from_wav = os.path.join(spk_dir, 'wav', '%s.wav' % trans_id)
            to_wav = os.path.join(out_spk_dir, 'wav', '%s.wav' % trans_id)
            
            shutil.copyfile(from_wav, to_wav)
            
        out_trans_writer.close()


def get_speakers_from_file(path):
    spks = []
    with open(path, 'r', encoding='utf-8') as reader:
        for line in reader:
            line = line.strip()
            spks.append(line)
    return spks


# def diff_trans_from_train(train_set, test_set):
#     # train set
#     train_transes = []

#     for  spk_id, data, spk_dir  in train_set:
#         for trans_id, trans in data:
#             train_transes.append(trans)

#     train_nb_trans = len(train_transes)

#     train_transes = list(set(train_transes))

#     # test set
#     test_transes = []

#     new_test_set = []
#     new_transes = []

#     for spk_id, data, spk_dir in test_set:
#         new_data = []
#         for ele in data:
#             trans = ele[1]
            
#             exist = False
#             for train_trans in train_transes:
#                 if trans == train_trans:
#                     exist = True
#                     break
            
#             if not exist:
#                 new_data.append(ele)
#                 new_transes.append(trans)

#             test_transes.append(trans)
            
#         new_test_set.append((spk_id, new_data, spk_dir))

#     print('Train set: total=%s, unique=%s' % (train_nb_trans, len(train_transes)))
#     print('Test set: total=%s, unique=%s' % (len(test_transes), len(list(set(test_transes)))))
#     print('Diff set: total=%s, unique=%s' % (len(new_transes), len(list(set(new_transes)))))

#     return new_test_set


if __name__ == '__main__':
    data_dir = '/home/sotheara.leang/phd/'

    # exclude_spks = get_speakers_from_file(data_dir + '/data/Khmer/BTEC/speaker_exclude.txt')
    # extract_dataset(data_dir=data_dir + '/data/Khmer/BTEC/NICT', output_dir=data_dir + '/data/Khmer/BTEC_EXP', exclude_spks=exclude_spks, clean=True)    

    # generate_train_test(data_dir=data_dir + '/data/Khmer/BTEC_EXP',
    #                     output_dir=data_dir + '/exp/data/khmer/ds', nb_test_spk=5, clean=True)

    generate_train_test2(data_dir=data_dir + '/data/Khmer/BTEC_EXP',
                        output_dir=data_dir + '/exp/data/khmer/ds', clean=True)

    generate_text_corpus(data_dir + '/data/Khmer/BTEC_EXP', output_dir=data_dir + '/exp/data/khmer/lm')