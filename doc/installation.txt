1. Install Miniconda

Follow the document to install miniconda: https://docs.conda.io/en/latest/miniconda.html

Basic conda command:
conda create --name kaldi python=3.7        :   create env
conda env create --file=environment.yml     :   create env from yml file
conda env remove -n kaldi                   :   remove env
conda env export > environment.yml          :   export env into yml file
conda env update --file local.yml --prune   :   update env from yml file
conda activate kaldi                        :   enter env
conda deactivate kaldi                      :   leave env

Install python packages from requirement.txt
pip install -r requirement.txt


2. Checkout Kaldi project

git clone https://github.com/kaldi-asr/kaldi

3. Install OS libraries

Check libraries to be installed
KALDI_ROOT/extra/check_dependencies

Basic libraries:
conda install -c conda-forge automake
conda install -c conda-forge autoconf
conda install -c conda-forge sox
conda install -c anaconda gawk

Intel MKL
conda install -c intel mkl
conda install -c intel mkl-include

Update MKL_ROOT in KALDI_ROOT/extra/check_dependencies to point to directory of mkl-include.

Make sure all required libraries are installed.

4. Install extra tools

SRILM for building language model: KALDI_ROOT/tool/install_srilm.sh <name> <organization> <email>

5. Compile Kaldi source codes

Build tool scripts
KALDI_ROOT/tool/make -j 4

Build src scripts
KALDI_ROOT/scr/configure --shared --mkl-root=/home/leangso/app/miniconda/pkgs/mkl-include-2021.4.0-intel_640 --mkl-libdir=/home/leangso/app/miniconda/pkgs/mkl-2021.4.0-intel_640/lib --use-cuda --cudatk-dir=/usr/local/cuda-10.2
KALDI_ROOT/scr/make clean depend
KALDI_ROOT/scr/make -j 4

touch $KALDI_ROOT/.done

6. Symlink to step and util folder of wsj

ln -s $KALDI_ROOT/egs/wsj/s5/steps $PROJ_DIR
ln -s $KALDI_ROOT/egs/wsj/s5/utils $PROJ_DIR

7. Notices about new version of Kaldi scripts
- Certificate problem for wget when building tool. We need to add --no-check-certificate in Makefile.
- Problem -C parameter of sort in validate_data_dir.sh. We need to change parameter from -C to -c.

8. Screen

conda install -c conda-forge screen
conda install -c conda-forge jupyterlab

screen -S name              :   create screen
screen -X -S name quit      :   remove screen
screen -r -d name           :   enter screen

